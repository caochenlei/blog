import * as React from 'react';
import { Avatar, Divider, Tooltip } from 'antd';
import { QqOutlined, WechatOutlined, SendOutlined } from '@ant-design/icons'
import { reqPersonal } from '../../axios'
import './Personal.less'
export interface PersonalProps {

}

export interface PersonalState {
  personal: {
    avatar: string
    email: string
    github: string
    nickname: string
    qq: string
    sign: string
    wechat: string
  }
}

class Personal extends React.Component<PersonalProps, PersonalState> {
  state = {
    personal: {
      avatar: '',
      email: '',
      github: '',
      nickname: '',
      qq: '',
      sign: '',
      wechat: ''
    }
  }
  async componentDidMount() {
    //请求博主信息
    const personalResponse = await reqPersonal();
    if (personalResponse.data.success) {
      this.setState({ personal: personalResponse.data.data });
    }
  }
  render() {
    const { personal } = this.state;
    return (
      <div className='author'>
        <Avatar className='avatar' shape="circle" size={100} src={personal.avatar} />
        <div className='author-name'>{personal.nickname}</div>
        <div className='author-speak'>
          {personal.sign}
        </div>
        <Divider>社交账号</Divider>
        <div className='author-tongxun'>
          <Tooltip placement="bottom" title={'QQ：' + personal.qq}>
            <Avatar icon={<QqOutlined />}></Avatar> &nbsp;
                </Tooltip>

          <Tooltip placement="bottom" title={'微信：' + personal.wechat}>
            <Avatar icon={<WechatOutlined />}></Avatar> &nbsp;
                </Tooltip>
          <Tooltip placement="bottom" title={'邮箱：' + personal.email}>
            <Avatar icon={<SendOutlined />}></Avatar> &nbsp;
                </Tooltip>
          {/* <Tooltip placement="bottom" title={personal.github}>
                <Avatar  icon={<WechatOutlined />}></Avatar> &nbsp;
                </Tooltip> */}
        </div>

      </div>
    );
  }
}

export default Personal;