import * as React from 'react';
import { Col, Row, Input, Menu, message } from 'antd'
import { HomeOutlined } from '@ant-design/icons';
import './Header.less'
import './blackFont.less'
import { NavLink, RouteComponentProps, withRouter } from 'react-router-dom';
import { reqCategorys, reqWeb } from '../../axios';
import IconFont from '../icon'
interface category {
  id: number
  icon: string
  name: string
  orderNum: 0
}
export interface HeaderProps extends RouteComponentProps {

}

export interface HeaderState {
  categorys: category[]
  web: {
    name: string
    logo: string
  }
}
const { Search } = Input
class Header extends React.Component<HeaderProps, HeaderState> {
  state = {
    categorys: [],
    web: {
      name: '',
      logo: ''
    }
  }
  header = React.createRef();
  async componentDidMount() {
    //请求分类信息
    const response = await reqCategorys({});
    this.setState({ categorys: response.data.data.records })
    //请求网站信息
    const webResponse = await reqWeb();
    if (webResponse.data.success) {
      this.setState({ web: webResponse.data.data });
    }
  }
  componentDidUpdate() {
    const path = this.props.history.location.pathname
    if (path.indexOf('/articleCateory') > -1 || path.indexOf('/articleDetail') > -1 || path.indexOf('/articleSearch') > -1) {
      //@ts-ignore
      this.header.current.classList.add('home-header-black')
    } else {
      //@ts-ignore
      this.header.current.classList.remove('home-header-black')
    }
  }
  //搜索文章
  onSearch = (searchValue: string) => {
    if (searchValue.trim() !== '') {
      this.props.history.push({ pathname: '/articleSearch', state: { searchValue } });
    } else {
      message.warn('搜索框不能为空哦！(づ￣3￣)づ╭❤～')
    }
  }
  render() {
    const { categorys, web } = this.state;

    return (
      // @ts-ignore
      <div ref={this.header} className='home-header' >
        <div className='header-center'>
          <Row align='bottom' justify='center'>
            <Col className='home-header-left' xs={24} sm={4} md={7} lg={5} xl={8} >
              <div className='header-logo'>
                <a href="http://localhost:3000">
                  <span className='web-name'>{web.name}</span>
                  <img src={web.logo} alt="logo" />
                </a>
              </div>
            </Col>
            <Col className='home-header-right' xs={0} sm={20} md={16} lg={18} xl={16} >
              {/* @ts-ignore */}
              <Search className='right-search' placeholder="找一找吧^_^" onSearch={this.onSearch} style={{ width: 200 }} />

              <Menu mode='horizontal'>
                <Menu.Item><HomeOutlined /><NavLink to='/home'>首页</NavLink></Menu.Item>
                {
                  categorys.map((category: category) => {
                    return (
                      <Menu.Item key={category.id}>
                        <NavLink to={`/articleCateory/${category.id}`}><IconFont type={category.icon} /> {category.name}</NavLink>
                      </Menu.Item>
                    )
                  })
                }
              </Menu>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

export default withRouter(Header);