import * as React from 'react';
import { List } from 'antd'
import { RouteComponentProps, withRouter } from 'react-router-dom'
import { CSSTransition, TransitionGroup } from 'react-transition-group';
import { article } from '../../page/Home/Home';
import moment from 'moment'
import MyMore from '../MyMore/MyMore';
import './ArticleList.less'
export interface ArticleListProps extends RouteComponentProps {
  header: any
  articles: article[]
  isAll?: Boolean
}

export interface ArticleListState {

}

class ArticleList extends React.PureComponent<ArticleListProps, ArticleListState> {

  render() {
    const { header, articles, isAll } = this.props;

    return (
      <div className='article-list'>
        {/* {loading === true ? <div className="example">
          <Spin size='large' />
        </div>:null} */}
        <List
          size="large"
          header={header}
          dataSource={articles}
          renderItem={item => (
            <TransitionGroup>
              <CSSTransition
                timeout={500}
                appear
                classNames='item'
              >
                <List.Item
                  key={item.article.id}
                >
                  <List.Item.Meta
                    avatar={<img
                      onClick={() => this.props.history.push(`/articleDetail/${item.article.id}`)}
                      width={272}
                      alt="logo"
                      src={`${item.article.cover}`}
                    />}
                  />
                  {
                    <div className='list-item-content'>
                      <div className='item-title'>
                        {item.article.isTop === 0 ? <span className='item-category'>置顶</span> : null}
                        {item.article.isOriginal === 0 ? <span className='item-category'>原创</span> : <span className='item-category'>转载</span>}
                        <span className='item-category'>{item.category.name}</span>
                        <h2 className='item-title-h2'>{item.article.title}</h2>
                      </div>
                      <div className='item-tags'>
                        {
                          item.tags.map(tag => {
                            return (
                              <span key={tag.id} className='item-tag'><i>#</i>{tag.content}</span>
                            )
                          })
                        }
                      </div>
                      <div className='item-brief'>{item.article.summary}</div>
                      <div className='item-detail'>
                        <span className='item-detail-fire'>阅读量：{item.article.read}</span>
                        <em className='item-detail-line'></em>
                        <span className='item-detail-good'>点赞数：{item.article.support}</span>
                        <em className='item-detail-line'></em>
                        <span className='item-detail-time'>发布时间：{moment(item.article.createTime).format('YYYY年MM月DD日')}</span>
                      </div>
                    </div>
                  }
                </List.Item>
              </CSSTransition>
            </TransitionGroup>

          )
          }
        />
        {!isAll ? <MyMore ></MyMore> :

          <div className='no-more'>
            <span className='no-more-hr'></span>
            <span className='no-more-text'>暂时只有这么多了</span>
            <span className='no-more-hr'></span>
          </div>}
      </div>
    );
  }
}

export default withRouter(ArticleList);