import * as React from 'react';
import Pubsub from 'pubsub-js'
import './MyMore.less'
export interface MyMoreProps {

}

export interface MyMoreState {

}

class MyMore extends React.PureComponent<MyMoreProps, MyMoreState> {
  // state = { :  }
  handleClick = () => {
    Pubsub.publish('clickCount', 1);
  }
  render() {
    return (
      <div className='my-more'>
        <div onClick={this.handleClick} className='more-btn'>加载更多</div>
      </div>
    );
  }
}

export default MyMore;