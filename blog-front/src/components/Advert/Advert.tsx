import * as React from 'react';
import './Advert.less'
export interface AdvertProps {
    
}
 
export interface AdvertState {
    
}
 
class Advert extends React.Component<AdvertProps, AdvertState> {
    
    render() { 
        return ( 
            <div className='advert'>
                <h2>广告</h2>
                <img src='https://online-exam-1256496288.cos.ap-beijing.myqcloud.com/blog-server/c20dbec619425fa63dfd6cb5edd6a7a8.png' alt="advert"/>
            </div>
         );
    }
}
 
export default Advert;