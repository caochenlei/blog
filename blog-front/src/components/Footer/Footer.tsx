import * as React from 'react';
import {reqWeb} from '../../axios/index'
import './Footer.less'
export interface FooterProps {

}

export interface FooterState {
    web: {
        copyright: string
        logo: string
    }
}

class Footer extends React.Component<FooterProps, FooterState> {
    state = { 
        web: {
            copyright: '',
            logo: ''
        }
      }
    async componentDidMount(){
        //请求网站信息
        const webResponse = await reqWeb();
        if (webResponse.data.success) {
            this.setState({ web: webResponse.data.data });
        }
    }
    render() {
        const {web } = this.state;
        return (<div className='footer'>
            <div className='footer-center'>
                <div className='footer-left'>
                    <div className='footer-logo'>
                        <img src={web.logo} alt="logo" />
                    </div>
                </div>
                <div className='footer-copyright'>
                    <div>{web.copyright}</div>
                </div>
            </div>
        </div>);
    }
}

export default Footer;