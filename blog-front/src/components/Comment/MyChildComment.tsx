import * as React from 'react';
import { Comment, Avatar } from 'antd';
import moment from 'moment'
import CommentText from '../CommentText/CommentText';

export interface MyChildCommentProps {
  children?: any
  author: string
  content: string
  id: null | number
  articleId: number
  parentId: number | null
  createTime: string | null
}

export interface MyChildCommentState {

}

class MyChildComment extends React.Component<MyChildCommentProps, MyChildCommentState> {
  state = { reply: false, id: null }
  reply = () => {
    const { reply } = this.state;
    this.setState({ reply: !reply, id: this.props.id })
  }
  hiddenReply = (flag: Boolean) => {
    this.setState({ reply: flag });
  }
  render() {
    const { reply } = this.state;
    const { children, author, content, parentId, createTime } = this.props;
    return (
      <>
        <Comment
          actions={parentId === null ? [<span>{moment(createTime).format('YYYY-MM-DD HH:MM')}</span>, <span key="comment-nested-reply-to" onClick={this.reply}>回复</span>] : [<span>{moment(createTime).format('YYYY-MM-DD HH:MM')}</span>]}
          author={author}
          avatar={
            <Avatar
              style={{ height: 30, width: 30 }}
              src="https://img0.baidu.com/it/u=2171530163,2991420920&fm=26&fmt=auto&gp=0.jpg"
              alt="avatar"
            />
          }
          content={
            <p>
              {content}
            </p>
          }
        >
          {reply ? <CommentText hiddenReply={this.hiddenReply} articleId={this.props.articleId} id={this.state.id}></CommentText> : null}
          {children}

        </Comment>

      </>
    );
  }
}

export default MyChildComment;