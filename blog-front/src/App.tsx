import { Route} from 'react-router-dom';
import Main from "./page/Main/Main";

function App() {
  return (
    <div className="App" >
        <Route path='/' component={Main}></Route>
    </div>
  );
}

export default App;
