import  Header  from '../../components/Header/Header'
import * as React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import Home from '../Home/Home';
import ArticleCateory from '../ArticleCateory/ArticleCateory';
import ArticleDetail from '../ArticleDetail/ArticleDetail';
import ArticleSearch from '../ArticleSearch/ArticleSearch';
import Footer from '../../components/Footer/Footer'
export interface MainProps {
    
}
 
export interface MainState {
    
}
 
class Main extends React.Component<MainProps, MainState> {
    // state = { :  }
    render() { 
        return ( 
            <div>
            
              <Header></Header>
              <div className='home'>
                <Switch>
                    <Route path='/home' component={Home}></Route>
                    <Route path='/articleCateory/:categoryId' component={ArticleCateory}></Route>
                    <Route path='/articleDetail/:id' component={ArticleDetail}></Route>
                    <Route path='/articleSearch' component={ArticleSearch}></Route>
                    <Redirect to='/home'></Redirect>
                </Switch>    
                </div>
            <Footer></Footer>
            
            </div>
            
         );
    }
}
 
export default Main;