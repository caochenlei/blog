import { Col, Menu, Row } from 'antd';
import * as React from 'react';
import Pubsub from 'pubsub-js';
import { RouteComponentProps } from 'react-router-dom';
import { reqArticles } from '../../axios';
import ArticleList from '../../components/ArticleList/ArticleList';
import { article } from '../Home/Home';
import './ArticleSearch.less'
export interface ArticleSearchProps extends RouteComponentProps {

}

export interface ArticleSearchState {
  searchValue: string
  articles: article[]
  clickCount: number
  articlesCount: number
  isAll: Boolean
}

class ArticleSearch extends React.PureComponent<ArticleSearchProps, ArticleSearchState> {
  state = {
    articles: [],
    searchValue: '',
    clickCount: 0,
    articlesCount: 0,
    isAll: false
  }
  async componentDidMount() {
    //注册订阅信息
    Pubsub.subscribe('clickCount', () => {
      //统计一下点击加载更多按钮的次数 为显示条数做准备
      this.setState({ clickCount: ++this.state.clickCount }, () => {
        this.searchArticles(searchValue, 'title');
      })
    })
    //@ts-ignore
    const searchValue = this.props.location.state.searchValue || '';
    this.setState({ searchValue }, () => {
      this.searchArticles(searchValue, 'title');
    })
  }
  componentDidUpdate() {
    //@ts-ignore
    const searchValue = this.props.location.state.searchValue || '';
    if (searchValue !== this.state.searchValue) {
      this.searchArticles(searchValue, 'title');
      this.setState({ searchValue });
    }
  }
  componentWillUnmount() {
    PubSub.unsubscribe('clickCount')
  }
  //根据条件查找文章
  searchArticles = async (searchValue: string, searchType: string, pageNum = 1, pageSize = 4) => {
    const { clickCount } = this.state;
    const response = await reqArticles({ searchEntity: { [searchType]: searchValue }, pageNum: pageNum + clickCount, pageSize, orderEntity: { 'is_top': 'asc', 'create_time': 'desc' } });
    if (response.data.success) {
      const articles = response.data.data.records;
      //记录文章的总数        
      this.setState({ articlesCount: response.data.data.total }, () => {
        if (this.state.articlesCount <= (pageNum + clickCount) * pageSize || this.state.articlesCount===0) {
          this.setState({ isAll: true })
        }
      })
      //修改文章数组
      if (clickCount === 0) {
        this.setState({ articles })
      } else {
        this.setState((state) => ({
          articles: [...state.articles, ...articles]
        }))
      }
    }
  }
  //根据字段进行搜索
  searchBy = (field: string) => {
    //@ts-ignore
    const searchValue = this.props.location.state.searchValue || '';
    this.setState({ clickCount: 0, isAll: false }, () => {
      this.searchArticles(searchValue, field);
    })
  }
  render() {
    const { articles, isAll } = this.state;
    const header = (
      <div>搜索结果</div>
    )
    return (
      <div className='articleSearch'>
        <div className='banner'>
          <div className='banner-background'></div>
        </div>
        <div className='search-page'>
          <Row>
            <Col className='search-left' span={6}>
              <div className='search-type'>
                <Menu defaultSelectedKeys={['title']}>
                  <Menu.Item key="title" onClick={() => this.searchBy('title')}>标题</Menu.Item>
                  <Menu.Item key="summary" onClick={() => this.searchBy('summary')}>简介</Menu.Item>
                </Menu>
              </div>
            </Col>
            <Col className='search-right' span={18}>
              <ArticleList isAll={isAll} articles={articles} header={header}></ArticleList>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

export default ArticleSearch;