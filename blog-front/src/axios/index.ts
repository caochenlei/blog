import ajax from "./ajax";

//查找所有的分类 参数为空为查找所有的分类
export const reqCategorys = (category:any) => ajax('/common/categorys/page',category,'post');
//获取所有的文章列表 参数为空为查找所有的分类
export const reqArticles = (articleSearch:{pageNum?:number,pageSize?:number,orderEntity?:{},searchEntity?:{}}) => ajax('/common/articles/page',articleSearch,'post');
//根据id获取文章内容
// export const reqArticle = (id:number) => ajax(`/common/articles/${id}`); ///common/articles/details/{id}
export const reqArticle = (id:number) => ajax(`/common/articles/details/${id}`); ///common/articles/details/{id}
//增加阅读量
export const reqIncrease = (increaseObj:string) => ajax('/common/articles/count',increaseObj,'put')
//根据评论id获取评论
export const reqCommentById = (commentId:number) =>ajax(`/common/comments/${commentId}`)
//添加评论
export const reqAddComment = (comment:{createTime?:Date,articleId:number,content:string,reviewerEmail:string,reviewerName:string,parentId:null|number}) =>ajax(`/common/comments`,comment,'post')

//根据id查找网站信息
export const reqWeb = () => ajax(`/common/websites/1`);
//根据id查找博主信息
export const reqPersonal = ()=> ajax('/common/userInfos/1')

//查找轮播图信息
export const reqCarousels = (articleSearch:{pageNum?:number,pageSize?:number,orderEntity?:{},searchEntity?:{}}) =>ajax(`/common/carousels/page`,articleSearch,'post');