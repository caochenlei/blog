/**
 * 请求工具类，对 axios 进一步封装处理
 */
import axios from 'axios'

const request = axios.create({
  baseURL: 'http://localhost:8080'
})


// 导出请求方法
export default request
