import * as React from 'react';
import { Input, Select } from 'antd';
import PubSub from 'pubsub-js';
import MyTag from '../Tag/MyTag';
import './ArticleComplete.less'
import MyUpload from '../MyUpload/MyUpload';
export interface ArticleCompleteProps {
    articleObj: any
}

export interface ArticleCompleteState {
    imageUrl: string,
    brief: string,
    isTop: number,
    isOriginal: number
}

class ArticleComplete extends React.Component<ArticleCompleteProps, ArticleCompleteState> {
    state = {
        imageUrl: '',
        brief: '',
        isTop: 0,
        isOriginal: 0
    };
    componentDidMount() {
        //回显数据
        if (this.props.articleObj.article) {
            const { article } = this.props.articleObj;
            this.setState({
                imageUrl: article.cover,
                brief: article.summary,
                isTop: article.isTop,
                isOriginal: article.isOriginal
            },()=>{
                const { imageUrl, brief, isTop, isOriginal } = this.state;
                PubSub.publish('imgUrlAndBrief', { imageUrl, brief, isTop, isOriginal })
            })
        }

    }
    //动态绑定简介
    inputChange = (e: any) => {
        this.setState({ brief: e.target.value }, () => {
            const { imageUrl, brief, isTop, isOriginal } = this.state;
            PubSub.publish('imgUrlAndBrief', { imageUrl, brief, isTop, isOriginal })
        })
    }
    handleSelect = (type: string, value: number) => {
        //@ts-ignore
        this.setState({ [type]: value }, () => {
            const { imageUrl, brief, isTop, isOriginal } = this.state;
            PubSub.publish('imgUrlAndBrief', { imageUrl, brief, isTop, isOriginal })
        })
    }
    //获取图片地址
    getImageUrl = (imageUrl:string) =>{
        this.setState({imageUrl})
    }
    //修改图片之后的回调，否则不会更新图片信息
    updateBackCall = () =>{
        const { imageUrl, brief, isTop, isOriginal } = this.state;
        PubSub.publish('imgUrlAndBrief', { imageUrl, brief, isTop, isOriginal })
    }

    render() {
        const {  imageUrl, isOriginal, isTop, brief } = this.state;

        return (
            <div className='article-model'>

                <div className='article-model-top'>
                    <div className='article-cover'>
                        请选择文章的封面
                        <MyUpload getImageUrl={this.getImageUrl} imageUrl={imageUrl} updateBackCall={this.updateBackCall}></MyUpload>
                    </div>
                    <div className='article-model-right'>
                        <div className='tags'>
                            <h3>文章标签</h3>
                            <MyTag articleObj={this.props.articleObj}></MyTag>
                        </div>

                        <div className='select'>
                            <>是否原创:</>
                            <Select value={isTop} onChange={(value: number) => this.handleSelect('isTop', value)}>
                                <Select.Option value={0}>置顶</Select.Option>
                                <Select.Option value={1}>取消置顶</Select.Option>
                            </Select>
                            <>是否置顶:</>
                            <Select value={isOriginal} onChange={(value: number) => this.handleSelect('isOriginal', value)}>
                                <Select.Option value={0}>原创</Select.Option>
                                <Select.Option value={1}>转载</Select.Option>
                            </Select>
                        </div>
                    </div>
                </div>
                <div className='article-brief'>
                    <h3>文章简介</h3>
                    <Input.TextArea value={brief} showCount maxLength={100} onChange={this.inputChange} />
                </div>

            </div>
        );
    }
}

export default ArticleComplete;