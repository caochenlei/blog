import { Input } from 'antd';
import './CarouselModal.less'
import * as React from 'react';
import { Icarousel } from '../../interface';
import MyUpload from '../MyUpload/MyUpload';
export interface CarouselModalProps {
    getCarousel: (carousel: Icarousel) => void
    carousel: Icarousel
}

export interface CarouselModalState {
    imageUrl: string,
    carouselName: string,
    carouselOrder: number,
    carouselHref: string,
    id?:number
}

class CarouselModal extends React.Component<CarouselModalProps, CarouselModalState> {
    state = {
        imageUrl: '',
        carouselName: '',
        carouselOrder: 0,
        carouselHref: '',
        id:0
    }
    componentDidMount(){
        const {addr,cover,name,orderNum,id}= this.props.carousel;
        this.setState({
            imageUrl:cover,
            carouselName:name,
            carouselOrder:orderNum,
            carouselHref:addr,
            id:id
        })
        
    }

    //收集文本输入的数据
    handleValue = (type: string, e: any) => {
        //@ts-ignore
        this.setState({ [type]: e.target.value }, () => {
            const { imageUrl, carouselName, carouselOrder, carouselHref,id } = this.state;
            this.props.getCarousel({ id,cover: imageUrl, name: carouselName, orderNum: carouselOrder, addr: carouselHref })
        });
    }
    //修改图片之后的回调，否则不会更新图片信息
    updateBackCall = () =>{
        const { imageUrl, carouselName, carouselOrder, carouselHref,id } = this.state;
        this.props.getCarousel({ id,cover: imageUrl, name: carouselName, orderNum: carouselOrder, addr: carouselHref })
    }
    //获取图片地址
    getImageUrl = (imageUrl:string) =>{        
        this.setState({imageUrl});
    }
    render() {
        const { imageUrl,carouselName,carouselOrder,carouselHref } = this.state;
        return (

            <div className='carousel-modal'>
                播图封面<MyUpload updateBackCall={this.updateBackCall} getImageUrl={this.getImageUrl} imageUrl={imageUrl}></MyUpload>
                轮播图名称<Input placeholder='轮播图名称' value={carouselName} onChange={(e: any) => this.handleValue('carouselName', e)}></Input>
                轮播图链接<Input placeholder='轮播图链接' value={carouselHref} onChange={(e: any) => this.handleValue('carouselHref', e)}></Input>
                轮播图排序<Input placeholder='轮播图排序' value={carouselOrder} onChange={(e: any) => this.handleValue('carouselOrder', e)}></Input>
            </div>

        );
    }
}

export default CarouselModal;