import * as React from 'react';
import {Menu} from 'antd';
import {route,Iroute} from '../../page/route/route'
import { NavLink } from 'react-router-dom';

export interface RouteLinkProps {
    mode: string
}
 
export interface RouteLinkState {
    
}
 
class RouteLink extends React.Component<RouteLinkProps, RouteLinkState> {
    // state = { :  }
    getMenuList = (routes:Iroute[] ) =>{
        return routes.map(route=>{
            if(!route.children){
                return  (<Menu.Item key={route.path}>
                    <NavLink to={route.path}> {route.title}</NavLink>
                </Menu.Item>)
            }else{
                return (
                    <Menu.SubMenu key={route.path} title={route.title}>
                        {
                            this.getMenuList(route.children)
                        }
                    </Menu.SubMenu>
                )
            }
        })
    }
    render() { 
        const {mode} = this.props;
        return ( 
           <>
            <Menu theme="dark" mode={mode==='horizontal'?'horizontal':'inline'} defaultSelectedKeys={['2']}>
                   {
                       this.getMenuList(route)
                   } 
            </Menu>
           </>
         );
    }
}
 
export default RouteLink;