import * as React from 'react';
import { Form, Button, Input, message } from 'antd';
import PubSub from 'pubsub-js';
import './CommentText.less'
import { reqAddComment } from '../../axios';
export interface CommentTextProps {
  id: number | null
  articleId: number
  hiddenReply?: (flag: Boolean) => void
}

export interface CommentTextState {

}

class CommentText extends React.Component<CommentTextProps, CommentTextState> {
  state = {
    submitting: false,
    reviewerName: '',
    reviewerEmail: '',
    content: '',
    createTime: '',
    id: null
  };

  handleSubmit = async () => {
    const { id, articleId } = this.props;
    const { content, reviewerName, reviewerEmail } = this.state;
    if (content.trim() === '' || reviewerName.trim() === '' || reviewerEmail.trim() === '') {
      message.warning("你的评论是不是少了点什么呢？(｀ﾍ´)=3");
    } else {
      const response = await reqAddComment({ reviewerName, reviewerEmail, content, articleId, parentId: id, createTime: new Date() });
      if (response.data.success) {
        this.props.hiddenReply === undefined ? console.log('123')
          : this.props.hiddenReply(false)
        this.setState({ content: '', reviewerName: '', reviewerEmail: '' })
        //请求成功时 发布 把添加的评论发送给评论列表
        PubSub.publish('comment', response.data.data)
        message.success(response.data.message)
      } else {
        message.error(response.data.message)
      }
    }

  };

  handleChange = (type: string, e: any) => {
    this.setState({ [type]: e.target.value });
  };
  render() {
    const { reviewerEmail, reviewerName, content } = this.state;
    return (
      <div className='comment-text'>
        <Form.Item>
          <Input.TextArea rows={4} value={content} onChange={(e: any) => this.handleChange('content', e)} />
        </Form.Item>
        <Form.Item className='comment-submit'>
          <Button type="primary" onClick={this.handleSubmit}>
            发表评论
          </Button>
          <Input placeholder='昵称(必填)' value={reviewerName} onChange={(e: any) => this.handleChange('reviewerName', e)}></Input>
          <Input placeholder='邮箱(不公开)' value={reviewerEmail} onChange={(e: any) => this.handleChange('reviewerEmail', e)}></Input>
        </Form.Item>
      </div>
    );
  }
}

export default CommentText;