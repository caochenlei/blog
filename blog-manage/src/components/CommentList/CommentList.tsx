import * as React from 'react';
import { Empty, message } from 'antd'
import PubSub from 'pubsub-js'
import MyComment from '../../components/Comment/MyComment';
import CommentText from '../CommentText/CommentText'
import { reqCommentsById } from '../../axios';

export interface CommentListProps {
  id: number
}
interface comment {
  id: number
  articleId: number
  createTime: string
  reviewerName: string
  parentId: number
  reviewerEmail: string
  content: string
  reply: comment[]
}
export interface CommentListState {
  comments: comment[],
  commentCount: number
}

class CommentList extends React.PureComponent<CommentListProps, CommentListState> {
  state = { comments: [], commentCount: 0 }
  commentCount: number = 0
  async componentDidMount() {
    //删除评论
    PubSub.subscribe('removeComment', (type: string, commentObj: { id: number, parentId: number }) => {
      const { id, parentId } = commentObj;
      if (!parentId) {
        this.setState((state) => ({
          comments: state.comments.filter(comment => comment.id !== id)
        }))
      } else {
        //@ts-ignore
        this.setState((state) => ({
          comments: state.comments.map((comment) => {
            if (comment.reply) {
              return {
                ...comment,
                reply: comment.reply.filter(c => c.id !== id)
              }
            } else {
              return comment
            }
          })
        }))
      }
      this.getCommentCount();
    })
    //新增评论
    PubSub.subscribe('comment', (type: any, comment: any) => {
      if (comment.parentId === null) {
        this.setState({ comments: [comment, ...this.state.comments] })
      } else {
        //@ts-ignore
        this.setState((state) => {
          return {
            comments: state.comments.map((c: comment) => {
              if (c.id === comment.parentId) {
                return {
                  ...c,
                  reply: [comment, ...c.reply || []]
                }
              } else {
                return c
              }
            })
          }
        })
      }
      this.getCommentCount();
    })
    //请求评论列表
    const response = await reqCommentsById(this.props.id);
    if (response.data.success) {
      this.setState({ comments: response.data.data }, this.getCommentCount)
    } else {
      message.error(response.data.message);
    }
  }
  componentWillUnmount(){
    PubSub.unsubscribe('removeComment');
    PubSub.unsubscribe('comment');
  }
  //计算出评论的数量
  getCommentCount = () => {
    this.commentCount=0;
    this.state.comments.forEach((comment: comment) => {
      this.commentCount++;
      if (comment.reply) {
        comment.reply.forEach(commentChild => {
          this.commentCount++;
        })
      }
    })
    this.setState({ commentCount: this.commentCount })
  }
  //遍历评论列表
  getCommentList = (comments: comment[]) => {
    return comments.map(comment => {
      if (comment.reply && comment.reply.length > 0) {
        return (
          <MyComment createTime={comment.createTime} parentId={comment.parentId} articleId={this.props.id} key={comment.id} id={comment.id} author={comment.reviewerName} content={comment.content}>
            {
              comment.reply.map(c => {
                return (<MyComment createTime={c.createTime} parentId={c.parentId} articleId={this.props.id} key={c.id} id={c.id} author={c.reviewerName} content={c.content}></MyComment>)
              })
            }
          </MyComment>
        )
      } else {
        return (<MyComment createTime={comment.createTime} parentId={comment.parentId} articleId={this.props.id} key={comment.id} id={comment.id} author={comment.reviewerName} content={comment.content}></MyComment>
        )
      }
    })
  }
  render() {
    const { comments, commentCount } = this.state;
    const { id } = this.props;
    return (<div>
      <div className='article-comment'>
        <div className='article-edit'>
          <CommentText articleId={id} id={null}></CommentText>
        </div>
        <div className='comment-title'>
          评论列表 ({commentCount}条评论)
                                </div>
        {commentCount === 0 ? <Empty></Empty> : this.getCommentList(comments)}
      </div>
    </div>);
  }
}

export default CommentList;