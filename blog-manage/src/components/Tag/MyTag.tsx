import * as React from 'react';
import { Tag, Input, Tooltip } from 'antd';
import PubSub from 'pubsub-js'; 
import {  PlusOutlined } from '@ant-design/icons';
import './MyTag.less'
export interface TagProps {
  articleObj:any
}

export interface TagState {
    tags: string[],
    editInputValue: string,
    editInputIndex: number,
    inputVisible: Boolean,
    inputValue: string
}

class MyTag extends React.Component<TagProps, TagState> {
    state = {
        tags: ['标签'],
        inputVisible: false,
        inputValue: '',
        editInputIndex: -1,
        editInputValue: '',
    }
    componentDidMount() {
      //回显标签数据
      if(this.props.articleObj.article){
        const tags = this.props.articleObj.tags.reduce((pre:any,tag:{content:string})=>{
            return [...pre,tag.content];
        },[])
        this.setState({tags},()=>{
          PubSub.publish('tags',this.state.tags)
        })
      }
    }

    handleClose = (removedTag:any) => {
        const tags = this.state.tags.filter(tag => tag !== removedTag);
        this.setState({ tags },()=>{
          PubSub.publish('tags',this.state.tags);
        });
        
        
      };
    
      showInput = () => {
        this.setState({ inputVisible: true }, () => this.input.focus());
      };
    
      handleInputChange = (e:any) => {
        this.setState({ inputValue: e.target.value });
      };
    
      handleInputConfirm = () => {
        const { inputValue } = this.state;
        let { tags } = this.state;
        if (inputValue && tags.indexOf(inputValue) === -1) {
          tags = [...tags, inputValue];
        }        
        this.setState({
          tags,
          inputVisible: false,
          inputValue: '',
        },()=>{
            PubSub.publish('tags',this.state.tags)
        });
      };
    
      handleEditInputChange = (e:any) => {
        this.setState({ editInputValue: e.target.value });
      };
    
      handleEditInputConfirm = () => {
        this.setState(({ tags, editInputIndex, editInputValue }) => {
          const newTags = [...tags];
          newTags[editInputIndex] = editInputValue;
    
          return {
            tags: newTags,
            editInputIndex: -1,
            editInputValue: '',
          };
        },()=>{
            PubSub.publish('tags',this.state.tags)
        });
        
      };
      input:any
      saveInputRef = (input:any) => {
        this.input = input;
      };
    editInput:any
      saveEditInputRef = (input:any) => {
        this.editInput = input;
      };
    render() {
        const {tags,editInputIndex,editInputValue,inputVisible,inputValue} = this.state
        return ( 
            <>
            {tags.map((tag, index) => {
          if (editInputIndex === index) {
            return (
              <Input
                ref={this.saveEditInputRef}
                key={tag}
                size="small"
                className="tag-input"
                value={editInputValue}
                onChange={this.handleEditInputChange}
                onBlur={this.handleEditInputConfirm}
                onPressEnter={this.handleEditInputConfirm}
              />
            );
          }

          const isLongTag = tag.length > 20;

          const tagElem = (
            <Tag
              className="edit-tag"
              key={tag}
              closable={true}
              onClose={() => this.handleClose(tag)}
            >
              <span
                onDoubleClick={e => {
                  if (index !== 0) {
                    this.setState({ editInputIndex: index, editInputValue: tag }, () => {
                      this.editInput.focus();
                    });
                    e.preventDefault();
                  }
                }}
              >
                {isLongTag ? `${tag.slice(0, 20)}...` : tag}
              </span>
            </Tag>
          );
          return isLongTag ? (
            <Tooltip title={tag} key={tag}>
              {tagElem}
            </Tooltip>
          ) : (
            tagElem
          );
        })}
        {inputVisible && (
          <Input
            ref={this.saveInputRef}
            type="text"
            size="small"
            className="tag-input"
            value={inputValue}
            onChange={this.handleInputChange}
            onBlur={this.handleInputConfirm}
            onPressEnter={this.handleInputConfirm}
          />
        )}
        {!inputVisible && (
          <Tag className="site-tag-plus" onClick={this.showInput}>
            <PlusOutlined /> New Tag
          </Tag>
        )}
            </>
         );
    }
}

export default MyTag;