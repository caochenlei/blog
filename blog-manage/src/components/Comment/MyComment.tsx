import * as React from 'react';
import { Comment, Avatar, message } from 'antd';
import moment from 'moment'
import './MyComment.less'
import CommentText from '../CommentText/CommentText';
import { reqRemoveComment } from '../../axios';

export interface MyCommentProps {
  children?: any
  author: string
  content: string
  id: null | number
  articleId: number
  parentId: number | null
  createTime: string | null
}

export interface MyCommentState {

}

class MyComment extends React.Component<MyCommentProps, MyCommentState> {
  state = { reply: false, id: null }
  reply = () => {
    const { reply } = this.state;
    this.setState({ reply: !reply, id: this.props.id })
  }
  hiddenReply = (flag: Boolean) => {
    this.setState({ reply: flag });
  }
  //删除评论
  removeComment = async() =>{
    const {id,parentId} = this.props;
    if(id){
    const response = await reqRemoveComment(id);
    if(response.data.success){
      PubSub.publish('removeComment',{id,parentId});
      message.success('评论删除成功了');
    }else{
      message.error(response.data.message);
    }
     
    }else{
      message.error('删除评论失败！')
    }

  }
  render() {
    const { reply } = this.state;
    const { children, author, content, parentId, createTime } = this.props;
    return (
      <>
        <div className='comment-item'>
          <Comment
            actions={
              parentId === null ? 
              [<span>{moment(createTime).format('YYYY-MM-DD HH:MM')}</span>, <span key="comment-nested-reply-to" onClick={this.reply}>回复</span>,<span onClick={this.removeComment}>删除</span>] 
              : 
              [<span>{moment(createTime).format('YYYY-MM-DD HH:MM')}</span>,<span onClick={this.removeComment}>删除</span>]
            }
            author={author}
            avatar={
              <Avatar
                src="https://img0.baidu.com/it/u=2171530163,2991420920&fm=26&fmt=auto&gp=0.jpg"
                alt="avatar"
              />
            }
            content={
              <p>
                {content}
              </p>
            }
          >
            {reply ? <CommentText hiddenReply={this.hiddenReply} articleId={this.props.articleId} id={this.state.id}></CommentText> : null}
            {children}

          </Comment>

        </div>
      </>
    );
  }
}

export default MyComment;