import { Redirect, Route, Switch } from 'react-router-dom';
import Login from './page/Login/Login';
import './App.less';
import Main from './page/Main/Main';
function App() {
  return (
    <div className="App">
      <Switch>
        <Route path='/login' component={Login}></Route>
        
        <Route path='/main' component={Main}></Route>
        <Redirect to='/login'></Redirect>
      </Switch>
    </div>
  );
}

export default App;
