//搜索条件
export interface search{
    "pageNum"?:number,
	"pageSize"?:number,
	"orderEntity"?:{},
	"searchEntity"?:{}
}
//文章列表
export interface article{
    article:{
        title:string,
        id:number,
        support:number,
        read:number,
        categoryId:number,
        content:string,
        cover: string,
        createTime:string,
        isOriginal:number,
        isTop:number,
        oppose:number,
        summary:string,
        updateTime:string
    },
    category:{
        name:string,
        icon:string,
        id:number,
        orderNum:number
    },
    tags:{
        content:string,
        id:number,
        articleId:number
    }[],
    comments:{}
}

//轮播图
export interface Icarousel{
    addr:string,
    cover:string,
    name:string,
    orderNum:number,
    id?:number
}

//博主信息
export interface Ipersonal{
    avatar:string
    email:string
    github:string
    id:number
    nickname:string
    qq:string
    sign:string
    wechat:string
}

//网站信息
export interface Iweb{
    id:number
    copyright:string
    name:string
    logo:string
}