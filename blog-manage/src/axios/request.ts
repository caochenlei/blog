/**
 * 请求工具类，对 axios 进一步封装处理
 */
import axios from 'axios'

const request = axios.create({
  baseURL: 'http://localhost:8080'
})

// 全局请求拦截
request.interceptors.request.use(function (config) {
  //@ts-ignore
  const user = window.localStorage.getItem('user')
  // const user = JSON.parse(window.localStorage.getItem('user'))

  // 如果有登录用户信息，则统一设置 token
  if (user) {
    // config.headers.Authorization = `Bearer ${user.token}`
    config.headers.Authorization = user

  }
  // console.log(config);
  return config
}, function (error) {
  return Promise.reject(error)
})

// 导出请求方法
export default request
