
import { Icarousel, Ipersonal, Iweb, search } from "../interface";
import { article } from "../page/AddArticle/AddArticle";
import ajax from "./ajax";

export const reqLogin = ({username,password}:{username:string,password:string}) => ajax('/login',{username,password},'post')
//查找所有的分类 参数为空为查找所有的分类
export const reqCategorys = (category:any) => ajax('/common/categorys/page',category,'post');
//添加分类
export const reqAddCategory = (category:{icon:string,name:string,orderNum:number}) => ajax('/categorys',category,'post');
//删除分类
export const reqRemoveCategory = (id:number) => ajax(`/categorys/${id}`,{},'delete')
//根据主键获取分类
export const reqGetCategoryById = (id:number) => ajax (`/common/categorys/${id}`)
//根据携带id更新分类
export const reqUpdateCategory = (category:{icon:string,name:string,orderNum:number,id:number}) => ajax('/categorys',category,'put')

//添加文章
export const reqAddArticle = ({article,tags}:{article:article,tags:{content:string}[]}) =>ajax('/articles',{article,tags},'post')
//获取文章列表
export const reqGetArticles = (articleSearch:search) => ajax('/common/articles/page',articleSearch,'post');
//删除文章
export const reqRemoveArticle = (id:number) => ajax(`/articles/${id}`,{},'delete');
//根据id获取文章
export const reqGetArticleById = (id:number) => ajax(`/common/articles/${id}`);
//根据id修改文章
export const reqUpdateArticle =  ({article,tags}:{article:article,tags:{content:string}[]}) =>ajax('/articles',{article,tags},'put')

//新增轮播图
export const reqAddCarousel = (carousel:Icarousel) =>ajax(`/carousels`,carousel,'post');
//修改轮播图
export const reqUpdateCarousel = (carousel:Icarousel) =>ajax(`/carousels`,carousel,'put');
//删除轮播图
export const reqRemoveCarousel = (id:number) => ajax(`/carousels/${id}`,{},'delete');
//获得轮播图列表
export const reqCarousels = (carousel:search) => ajax(`/common/carousels/page`,carousel,'post');
//根据id查找轮播图信息
export const reqCarousel = (id:number) =>ajax(`/common/carousels/${id}`);

//修改博主信息
export const reqUpdatePersonal = (personal:Ipersonal) =>ajax('/userInfos',personal,'put');
//根据id获取博主信息
export const reqPersonalById = (id:number) => ajax(`/common/userInfos/${id}`)

//修改网站信息
export const reqUpdateWeb = (web:Iweb) =>ajax('/websites',web,'put');
//根据id获取网站信息
export const reqWebById = (id:number) => ajax(`/common/websites/${id}`);

//上传图片
export const reqUpload = 'http://39.105.24.185/upload';

//根据评论id获取评论
export const reqCommentById = (commentId:number) =>ajax(`/common/comments/${commentId}`)
//添加评论
export const reqAddComment = (comment:{createTime?:Date,articleId:number,content:string,reviewerEmail:string,reviewerName:string,parentId:null|number}) =>ajax(`/common/comments`,comment,'post')
//根据文章id获取评论
export const reqCommentsById = (articleId:number) => ajax(`/common/comments/findAllCommentsByArticleId?articleId=${articleId}`,{},'post')
//根据评论id删除评论
export const reqRemoveComment = (commentId:number) => ajax(`/comments/${commentId}`,{},'delete');