import request from './request'
export default function ajax(url: string = '', data = {}, method: string = 'get') {
    if (method === 'get') {
        return request.get(url)
    } else if (method === 'delete') {
        return request.delete(url)
    }else if(method === 'put'){
        return request.put(url, data)
    }
    else {
        return request.post(url, data)
    }
}