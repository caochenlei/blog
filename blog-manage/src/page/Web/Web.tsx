import { Button, Input, message } from 'antd';
import * as React from 'react';
import { reqUpdateWeb, reqWebById } from '../../axios';
import MyUpload from '../../components/MyUpload/MyUpload';
import { Iweb } from '../../interface';
import './Web.less'
export interface WebProps {
    
}
 
export interface WebState extends Iweb{
    
}
 
class Web extends React.Component<WebProps, WebState> {
    state = { 
        copyright:'',
        name:'',
        logo:'',
        id:1  
    }
    async UNSAFE_componentWillMount(){
        //获取网站信息
        const response = await reqWebById(1);
        if(response.data.success){
            const {copyright,name,logo} = response.data.data;
            this.setState({copyright,name,logo})
        }else{
            message.error(response.data.message);
        }
    }
    //获取图片链接 
    getImageUrl = (imageUrl:string)=>{
        this.setState({logo:imageUrl})        
    }
    //动态绑定
    handleValue = (type:string,e:any)=>{
        //@ts-ignore
        this.setState({[type]: e.target.value});
    }
    //修改网站数据
    updateWeb = async() =>{
        const response = await reqUpdateWeb(this.state);
        if(response.data.success){
            message.success(response.data.message);
        }else{
            message.error(response.data.message);
        }
    }
    render() { 
        const {copyright,name,logo} = this.state;
        
        return ( 
            <div className='web'>
                logo : <MyUpload getImageUrl={this.getImageUrl} imageUrl={logo} />
                网站名称: <Input value={name} onChange={(e)=>this.handleValue('name',e)}/>
                版权信息: <Input.TextArea value={copyright} onChange={(e)=>this.handleValue('copyright',e)}></Input.TextArea>
                <Button type='primary' onClick={this.updateWeb}>修改网站信息</Button>
            </div>
         );
    }
}
 
export default Web;