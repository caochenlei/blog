import * as React from 'react';
import { Layout, Breadcrumb } from 'antd';
import { Switch, Redirect, Route } from 'react-router-dom'
import RouteLink from '../../components/RouteLink/RouteLink'
import AddArticle from '../AddArticle/AddArticle';
import ArticleList from '../ArticleList/ArticleList';
import CategoryList from '../CategoryList/CategoryList';
import './Main.less'
import CarouselList from '../CarouselList/CarouselList';
import Personal from '../Personal/Personal';
import Web from '../Web/Web';
import MyFooter from '../../components/Footer/MyFooter';

export interface MainProps {

}

export interface MainState {

}
const { Header, Content } = Layout;
class Main extends React.Component<MainProps, MainState> {
    render() {
        return (
            <div className='blogManageMain'>
                
                    <Layout className="layout">
                        <Header>
                            <RouteLink mode='horizontal' />
                        </Header>
                        <Content style={{ margin: '10px 50px', border: '5px soild #f0f2f5' }}>
                            <Breadcrumb style={{ margin: '16px 0' }}>
                                {/* <Breadcrumb.Item>Home</Breadcrumb.Item>
                                <Breadcrumb.Item>List</Breadcrumb.Item>
                                <Breadcrumb.Item>App</Breadcrumb.Item> */}
                            </Breadcrumb>
                            <div className="site-layout-content">
                                <Switch>
                                    <Route path='/main/article/add' component={AddArticle}></Route>
                                    <Route path='/main/article/list' component={ArticleList}></Route>
                                    <Route path='/main/category' component={CategoryList}></Route>
                                    <Route path='/main/carousel' component={CarouselList}></Route>
                                    <Route path='/main/personal' component={Personal}></Route>
                                    <Route path='/main/web' component={Web}></Route>
                                    <Redirect to='/main/article/add'></Redirect>
                                </Switch>
                            </div>
                        </Content>
                        <MyFooter></MyFooter>
                    </Layout>
                   
            </div>
        );
    }
}

export default Main;