import * as React from 'react';
import { Button, Col, List, message, Row,Modal } from 'antd';
import { ExclamationCircleOutlined } from '@ant-design/icons';
import CarouselModal from '../../components/CarouselModal/CarouselModal';
import { Icarousel } from '../../interface';
import { reqAddCarousel, reqCarousel, reqCarousels, reqRemoveCarousel, reqUpdateCarousel } from '../../axios';
import './CarouselList.less'
export interface CarouselListProps {

}

export interface CarouselListState {
    isCarouselModel: Boolean
    carousels: Icarousel[]
    isAdd: Boolean
}

class CarouselList extends React.Component<CarouselListProps, CarouselListState> {
    carousel: Icarousel = { addr: '', name: '', cover: '', orderNum: 0 }
    state = {
        isCarouselModel: false,
        carousels: [
            {
                id: 0,
                addr: '',
                name: '',
                cover: '',
                orderNum: 0
            }
        ],
        isAdd: true
    }
    async componentDidMount() {
        const response = await reqCarousels({ orderEntity: {} });
        if (response.data.success) {
            this.setState({ carousels: response.data.data.records })
        }
    }

    //  添加或修改轮播图
    onOk = async () => {
        const { isAdd } = this.state;
        if (isAdd) {
            //添加文章
            const response = await reqAddCarousel(this.carousel)
            if (response.data.success) {
                message.success(response.data.message);
                this.setState((state) => ({
                    isCarouselModel: false,
                    carousels: [...state.carousels, response.data.data]
                }));
            } else {
                message.error(response.data.message);
            }
        } else {
            //修改文章
            const response = await reqUpdateCarousel(this.carousel);
            if (response.data.success) {                
                message.success(response.data.message);
                this.setState((state) => ({
                    isCarouselModel: false,
                    carousels: state.carousels.map(carousel => {
                        if (carousel.id ===this.carousel.id) {
                            return this.carousel
                        } else {
                            return carousel
                        }
                    })
                }));
            } else {
                message.error(response.data.message);
            }
        }
    }
    //取消对话框
    onCancel = () => {
        this.setState({ isCarouselModel: false });
    }
    //根据id查找轮播图
    getCarouselById = async (id: number) => {
        const response = await reqCarousel(id);
        if (response.data.success) {
            this.carousel = {...response.data.data,id};
            this.setState({ isCarouselModel: true, isAdd: false })
        }
    }
    //根据id删除轮播图
    removeCarouselById = (id:number)=>{
        const _this = this;
        Modal.confirm({
            title: '确定要删除此轮播图吗？',
            icon: <ExclamationCircleOutlined />,
            content: '该操作不可逆！',
            async onOk() {
                const response= await reqRemoveCarousel(id);
                if(response.data.success){
                    message.success(response.data.message);
                    _this.setState((state)=>({
                        // ...state,
                        carousels:state.carousels.filter(carousel=>carousel.id!==id)
                    }))
                }else{
                    message.error(response.data.message);
                }
            }
          });
        
    }
    render() {
        const { isCarouselModel, carousels } = this.state;
        return (<div className='carousel-list'>
            <div className='add-btn'><Button type='primary' onClick={() => {this.setState({ isCarouselModel: true, isAdd: true, });this.carousel={ addr: '', name: '', cover: '', orderNum: 0 }}}>添加轮播图</Button></div>
            <List
                header={<div>
                    <Row>
                        <Col span={4}>
                            <span>轮播图名称</span>
                        </Col>
                        <Col span={4}>
                            <span>轮播图排序</span>
                        </Col>
                        <Col span={4}>
                            <span>图片</span>
                        </Col>
                        <Col span={6}>
                            <span>链接地址</span>
                        </Col>
                        <Col span={6}>
                            <span>操作</span>
                        </Col>
                    </Row>
                </div>}
                bordered
                dataSource={carousels}
                renderItem={item => (
                    <List.Item>
                        <Row style={{ width: '100%' }}>
                            <Col span={4}>
                                {item.name}
                            </Col>
                            <Col span={4}>
                                {item.orderNum}
                            </Col>
                            <Col span={4}>
                                <img style={{ height: 50, width: 50 }} src={item.cover} alt="img" />
                            </Col>
                            <Col span={6}>
                                {item.addr}
                            </Col>
                            <Col span={6}>
                                <Button type='primary' onClick={() => this.getCarouselById(item.id)} style={{marginRight:10}}>修改</Button>
                                <Button type='primary' onClick={() => this.removeCarouselById(item.id)} danger>删除</Button>
                            </Col>
                        </Row>
                    </List.Item>
                )}
            />
            <Modal destroyOnClose onOk={this.onOk} onCancel={this.onCancel} visible={isCarouselModel ? true : false}>
                <CarouselModal carousel={this.carousel} getCarousel={(carousel: Icarousel) => this.carousel = carousel}></CarouselModal>
            </Modal>
        </div>);
    }
}

export default CarouselList;