import { Button, Input, message } from 'antd';
import * as React from 'react';
import { Ipersonal } from '../../interface';
import { reqPersonalById, reqUpdatePersonal } from '../../axios';
import MyUpload from '../../components/MyUpload/MyUpload';
import './Personal.less';
export interface PersonalProps {

}

export interface PersonalState {
    personal: Ipersonal
}

class Personal extends React.Component<PersonalProps, PersonalState> {
    state = {
        personal: {
            avatar: '',
            email: '',
            github: '',
            id: 1,
            nickname: '',
            qq: '',
            sign: '',
            wechat: ''
        }
    }
    async componentDidMount() {
        //查询博主信息
        const response = await reqPersonalById(1);
        if (response.data.success) {
            this.setState({ personal: response.data.data });
        } else {
            message.error(response.data.message);
        }
    }

    //动态绑定文本框
    handleValue = (type: string, e: any) => {
        //@ts-ignore
        this.setState((state) => ({
            personal: { ...state.personal, [type]: e.target.value }
        }));
    }
    //修改博主信息
    updatePersonal = async () => {
        const { personal } = this.state;
        const response = await reqUpdatePersonal(personal);
        if (response.data.success) {
            message.success(response.data.message);
        }
    }
    //获取图片地址
    getImageUrl = (imageUrl: string) => {
        this.setState((state) => ({
            personal: { ...state.personal, avatar: imageUrl }
        }))
    }
    render() {
        const { personal } = this.state;
        return (
            <div className='personal'>
                <div> 头像：<MyUpload imageUrl={personal.avatar} getImageUrl={this.getImageUrl}></MyUpload></div>
                <div>用户名：<Input value={personal.nickname} onChange={(e) => this.handleValue('nickname', e)}></Input></div>
                <div> 留言： <Input.TextArea value={personal.sign} onChange={(e) => this.handleValue('sign', e)}></Input.TextArea></div>
                <div>电子邮箱: <Input value={personal.email} onChange={(e) => this.handleValue('email', e)} /></div>
                <div> QQ: <Input value={personal.qq} onChange={(e) => this.handleValue('qq', e)} /></div>
                <div>微信: <Input value={personal.wechat} onChange={(e) => this.handleValue('wechat', e)} /></div>
                <div> github: <Input value={personal.github} onChange={(e) => this.handleValue('github', e)} /></div>
                <Button type='primary' onClick={this.updatePersonal}>修改</Button>
            </div>
        );
    }
}

export default Personal;