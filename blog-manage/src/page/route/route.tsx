import { ReactNode } from "react";
import AddArticle from "../AddArticle/AddArticle";
import ArticleList from "../ArticleList/ArticleList";
import CategoryList from "../CategoryList/CategoryList";

export interface Iroute  {
    title:string,
    children ?: Iroute[] ,
    path : string,
    component ?: ReactNode,
    icon ?: any


}
export const route:Iroute[] = [
    {
        title: '分类管理',
        path : '/main/category',
        component: <CategoryList/>

    },
    {
        title:'文章管理',
        path: '/main/article',
        children:[
            {
                title: '文章添加',
                path: '/main/article/add',
                //@ts-ignore
                component: <AddArticle/>
            },{
                title: '文章列表',
                path : '/main/article/list',
                //@ts-ignore
                component: <ArticleList/>
            }
        ]
    },{
        title: '轮播图管理',
        path: '/main/carousel',
    },{
        title: '博主信息管理',
        path: '/main/personal',
    },{
        title: '网站信息管理',
        path: '/main/web',
    }
]