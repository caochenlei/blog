import * as React from 'react';
import { Table, Tag, Space, Modal, message, Button } from 'antd';
import { reqGetArticleById, reqGetArticles, reqRemoveArticle } from '../../axios';
import { article } from '../../interface';
import { ExclamationCircleOutlined } from '@ant-design/icons';
import { RouteComponentProps } from 'react-router-dom';
import CommentList from '../../components/CommentList/CommentList';
export interface ArticleProps extends RouteComponentProps{

}

export interface ArticleState {
  articles: article[]
  data: any
  showComments: Boolean
  articleId:number
}

class ArticleList extends React.Component<ArticleProps, ArticleState> {
  state = {
    articles: [
      {
        article: {
          title: '',
          id: 0,
          support: 0,
          read: 0,
          categoryId: 0,
          content: '',
          cover: '',
          createTime: '',
          isOriginal: 0,
          isTop: 0,
          oppose: 0,
          summary: '',
          updateTime: ''
        },
        category: {
          name: '',
          icon: '',
          id: 0,
          orderNum: 0
        },
        tags: [
          {
            content: '',
            id: 0,
            articleId: 0
          }
        ],
        comments: {}
      }
    ],
    data: [{
      id: 0,
      title: '',
      category: '',
      readCount: 0,
      tags: [{ content: '' }],
      good: 0
    }],
    showComments:false,
    articleId:0
  }
  async componentDidMount() {
    const response = await reqGetArticles({});
    if (response.data.success) {
      this.setState({ articles: response.data.data.records }, () => {
        const data = this.state.articles.map(articleRow => {
          const { article, category, tags } = articleRow;
          return {
            title: article.title,
            id: article.id,
            category: category.name,
            readCount: article.read,
            tags: tags,
            good: article.support
          }
        })
        this.setState({ data })
      })
    }
  }
  //根据id获取文章
  update = async (id: number) => {
    const response = await reqGetArticleById(id);
    if (response.data.success) {
      const articleObj =response.data.data
      //@ts-ignore
      this.props.history.push({pathname:'/main/article/add',state:{articleObj}});
    }
  }
  //删除文章
  showDeleteConfirm = (id: number) => {
    const _this = this
    Modal.confirm({
      title: '确认要删除吗？',
      icon: <ExclamationCircleOutlined />,
      content: '该操作不可逆！',
      okText: 'Yes',
      okType: 'danger',
      cancelText: 'No',
      async onOk() {
        const response = await reqRemoveArticle(id);
        if (response.data.success) {
          _this.setState((state) => {
            return {
              data: state.data.filter((data: any) => data.id !== id)
            }
          })
          message.success(response.data.message);
        } else {
          message.error(response.data.message);
        }

      },
      onCancel() {
        console.log('Cancel');
      },
    });
  }
  //显示评论列表
  showComments = (flag:Boolean,id:number) =>{
    this.setState({articleId:id,showComments:flag})
  }
  //确认评论对话框
  handleOk = () =>{
    this.setState({showComments:false})
  }
   //取消评论对话框
   handleCancel = () =>{
    this.setState({showComments:false})
  }


  render() {
    const { data,showComments,articleId } = this.state;
    const columns = [
      {
        title: '文章标题',
        dataIndex: 'title',
      },
      {
        title: '文章分类',
        dataIndex: 'category',
      },
      {
        title: '阅读量',
        dataIndex: 'readCount',
      },
      {
        title: '点赞',
        dataIndex: 'good',
      },
      {
        title: '标签',
        dataIndex: 'tags',
        render: (tags: any) => (
          <>
            {tags.map((tag: any) => {
              return (
                <Tag color={'geekblue'} key={tag.content}>
                  {tag.content.toUpperCase()}
                </Tag>
              );
            })}
          </>
        ),
      },
      {
        title: '评论管理',
        render: (text:any)=>(
          <Button type='primary' onClick={()=> this.showComments(true,text.id)}>查看评论</Button>
        )
      },
      {
        title: '操作',
        key: 'action',
        render: (text: any) => (
          <Space size="middle">
            <Button type='primary' onClick={() => this.update(text.id)}>修改 </Button>
            <Button type='primary' danger onClick={() => this.showDeleteConfirm(text.id)}>删除</Button>
          </Space>
        ),
      },
    ];

    return (
      <div>
        <Table rowKey='id' columns={columns} dataSource={data} />
        <Modal destroyOnClose title="Basic Modal" visible={showComments} onOk={this.handleOk} onCancel={this.handleCancel}>
        <CommentList id= {articleId}></CommentList>
      </Modal>
      </div>
    );
  }
}

export default ArticleList;