import * as React from 'react';
import {message} from 'antd'
import {reqLogin} from '../../axios'
import { Form, Input, Button } from 'antd';

import './Login.less'
import { RouteComponentProps } from 'react-router-dom';
export interface LoginProps extends RouteComponentProps{

}

export interface LoginState {

}

class Login extends React.Component<LoginProps, LoginState> {

  render() {
    const layout = {
      labelCol: { span: 4 },
      wrapperCol: { span: 20 },
    };
    const tailLayout = {
      wrapperCol: { offset: 4, span: 20 },
    };
    //表单验证通过
    const onFinish = async (values: any) => {
      const {username,password} = values;
      try {
        const response =  await reqLogin({username,password});
        if(response.data.success){
          message.success(response.data.message);
          window.localStorage.setItem('user',response.data.data);
          this.props.history.push('/main');
        }else{
          message.error(response.data.message);
        } 
      } catch (error) {
        console.log(error);
      }
    };
    //表单验证失败
    const onFinishFailed = (errorInfo: any) => {
      console.log('Failed:', errorInfo);
    };
    return (
      <div className='login'>
        <div className='login-header'>
          <h2>个人博客管理系统</h2>
        </div>
        <div className='login-box'>
          <h2>管理员登录</h2>
          <Form
            {...layout}
            name="basic"
            initialValues={{ remember: false }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
          >
            <Form.Item
              label="用户名"
              name="username"
              rules={[{ required: true, message: '请输入你的用户名' }]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              style={{color:' #fff'}}
              label="密码"
              name="password"
              rules={[{ required: true, message: '请输入你的密码' }]}
            >
              <Input.Password />
            </Form.Item>

            <Form.Item {...tailLayout}>
              <Button  block  htmlType="submit">
                登录
        </Button>
            </Form.Item>
          </Form>
        </div>
      </div>
    );
  }
}

export default Login;