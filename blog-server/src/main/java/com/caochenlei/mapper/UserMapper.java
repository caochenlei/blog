package com.caochenlei.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.caochenlei.domain.User;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMapper extends BaseMapper<User> {

}
