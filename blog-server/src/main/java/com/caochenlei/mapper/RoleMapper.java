package com.caochenlei.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.caochenlei.domain.Role;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RoleMapper extends BaseMapper<Role> {

}
