package com.caochenlei.mapper;

import com.caochenlei.domain.SysRole;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface SysRoleMapper {
    //根据用户编号查询角色列表
    @Select("select * from `sys_role` where id in (" +
            "  select rid from `sys_user_role` where uid = #{uid}" +
            ")")
    List<SysRole> findByUid(Integer uid);
}