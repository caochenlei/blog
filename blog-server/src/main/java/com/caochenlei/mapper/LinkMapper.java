package com.caochenlei.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.caochenlei.domain.Link;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface LinkMapper extends BaseMapper<Link> {

}
