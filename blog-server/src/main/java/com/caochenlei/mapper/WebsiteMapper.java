package com.caochenlei.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.caochenlei.domain.Website;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface WebsiteMapper extends BaseMapper<Website> {

}
