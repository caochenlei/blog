package com.caochenlei.exception;

import com.caochenlei.domain.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler(AccessDeniedException.class)
    public Result accessDeniedException() {
        log.info("用户权限不足！");
        return new Result(false, null, "用户权限不足！");
    }

    @ExceptionHandler(RuntimeException.class)
    public Result serverException(Exception e) {
        log.error(e.getMessage());
        e.printStackTrace();
        return new Result(false, null, "服务出现异常！");
    }
}