package com.caochenlei.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class WebApiConfig {
    @Bean
    public Docket docket() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.caochenlei.controller"))
                .paths(PathSelectors.any())
                .build();
    }

    //配置项目基本信息
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("个人博客系统接口文档")
                .description("华北理工大学接计开发小组")
                .termsOfServiceUrl("https://gitee.com/caochenlei/blog-server")
                .contact(new Contact("caochenlei & yushengshi", "https://caochenlei.blog.csdn.net/", "774908833@qq.com"))
                .version("1.0")
                .build();
    }
}
