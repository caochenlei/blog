package com.caochenlei.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.caochenlei.domain.Comment;

import java.util.Map;

public interface CommentService {
    //插入一条记录
    public Comment insert(Comment comment);

    //根据主键删除
    public Integer deleteById(Integer id);

    //根据主键修改
    public Integer updateById(Comment comment);

    //根据主键查找
    public Comment selectById(Integer id);

    //根据条件查找
    public Page<Comment> search(Map<String,Object> searchMap);
}
