package com.caochenlei.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.caochenlei.domain.Article;
import com.caochenlei.domain.ArticleVo;

import java.util.Map;

public interface ArticleService {
    //插入一条记录
    public ArticleVo insert(ArticleVo articleVo);

    //根据主键删除
    public Integer deleteById(Integer id);

    //根据主键修改
    public Integer updateById(ArticleVo articleVo);

    //根据主键查找
    public ArticleVo selectById(Integer id);

    //根据条件查找
    public Page<ArticleVo> search(Map<String, Object> searchMap);

    //更新数量状态
    Integer updateCount(Integer articleId, String filedName, Long count);
}
