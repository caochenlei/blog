package com.caochenlei.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.caochenlei.domain.UserRole;
import com.caochenlei.mapper.UserRoleMapper;
import com.caochenlei.service.UserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;
import java.util.Set;

@Service
@Transactional
public class UserRoleServiceImpl implements UserRoleService {
    @Autowired(required = false)
    private UserRoleMapper userRoleMapper;

    @Override
    public UserRole insert(UserRole userRole) {
        userRoleMapper.insert(userRole);
        return userRole;
    }

    @Override
    public Integer deleteById(Integer uid) {
        return userRoleMapper.deleteById(uid);
    }

    @Override
    public Integer updateById(UserRole userRole) {
        return userRoleMapper.updateById(userRole);
    }

    @Override
    public UserRole selectById(Integer uid) {
        return userRoleMapper.selectById(uid);
    }

    @Override
    public Page<UserRole> search(Map<String, Object> searchMap) {
        //定义查询参数
        QueryWrapper queryWrapper = new QueryWrapper();
        Integer pageNum = 1;
        Integer pageSize = 20;
        Map<String, String> orderEntity = null;
        Map<String, String> searchEntity = null;
        //接收查询参数
        if (searchMap.get("pageNum") != null) {
            pageNum = (Integer) searchMap.get("pageNum");
        }
        if (searchMap.get("pageSize") != null) {
            pageSize = (Integer) searchMap.get("pageSize");
        }
        if (searchMap.get("orderEntity") != null) {
            orderEntity = (Map<String, String>) searchMap.get("orderEntity");
            Set<Map.Entry<String, String>> OrderEntries = orderEntity.entrySet();
            for (Map.Entry<String, String> entry : OrderEntries) {
                String orderByColumn = entry.getKey();
                String orderByValue = entry.getValue();
                if ("asc".equals(orderByValue)) {
                    queryWrapper.orderByAsc(orderByColumn);
                } else {
                    queryWrapper.orderByDesc(orderByColumn);
                }
            }
        }
        if (searchMap.get("searchEntity") != null) {
            searchEntity = (Map<String, String>) searchMap.get("searchEntity");
            //查询主键列
            if (searchEntity.get("uid") != null) {
                queryWrapper.eq("uid", searchEntity.get("uid"));
            }
            //查询普通列
        }
        //创建分页对象
        Page<UserRole> page = new Page<>(pageNum, pageSize);
        //返回查询结果
        Page<UserRole> userRolePage = userRoleMapper.selectPage(page, queryWrapper);
        return userRolePage;
    }
}
