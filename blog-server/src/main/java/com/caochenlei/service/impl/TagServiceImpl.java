package com.caochenlei.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.caochenlei.domain.ArticleVo;
import com.caochenlei.domain.Tag;
import com.caochenlei.mapper.ArticleMapper;
import com.caochenlei.mapper.TagMapper;
import com.caochenlei.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
@Transactional
public class TagServiceImpl implements TagService {
    @Autowired(required = false)
    private TagMapper tagMapper;
    @Autowired(required = false)
    private ArticleMapper articleMapper;

    @Override
    public Tag insert(Tag tag) {
        tagMapper.insert(tag);
        return tag;
    }

    @Override
    public Integer deleteById(Integer id) {
        return tagMapper.deleteById(id);
    }

    @Override
    public Integer updateById(Tag tag) {
        return tagMapper.updateById(tag);
    }

    @Override
    public Tag selectById(Integer id) {
        return tagMapper.selectById(id);
    }

    @Override
    public Page<Tag> search(Map<String, Object> searchMap) {
        //定义查询参数
        QueryWrapper queryWrapper = new QueryWrapper();
        Integer pageNum = 1;
        Integer pageSize = 20;
        Map<String, String> orderEntity = null;
        Map<String, String> searchEntity = null;
        //接收查询参数
        if (searchMap.get("pageNum") != null) {
            pageNum = (Integer) searchMap.get("pageNum");
        }
        if (searchMap.get("pageSize") != null) {
            pageSize = (Integer) searchMap.get("pageSize");
        }
        if (searchMap.get("orderEntity") != null) {
            orderEntity = (Map<String, String>) searchMap.get("orderEntity");
            Set<Map.Entry<String, String>> OrderEntries = orderEntity.entrySet();
            for (Map.Entry<String, String> entry : OrderEntries) {
                String orderByColumn = entry.getKey();
                String orderByValue = entry.getValue();
                if ("asc".equals(orderByValue)) {
                    queryWrapper.orderByAsc(orderByColumn);
                } else {
                    queryWrapper.orderByDesc(orderByColumn);
                }
            }
        }
        if (searchMap.get("searchEntity") != null) {
            searchEntity = (Map<String, String>) searchMap.get("searchEntity");
            //查询主键列
            if (searchEntity.get("id") != null) {
                queryWrapper.eq("id", searchEntity.get("id"));
            }
            //查询普通列
            if (searchEntity.get("article_id") != null) {
                queryWrapper.like("article_id", searchEntity.get("article_id"));
            }
            if (searchEntity.get("content") != null) {
                queryWrapper.like("content", searchEntity.get("content"));
            }
        }
        //创建分页对象
        Page<Tag> page = new Page<>(pageNum, pageSize);
        //返回查询结果
        Page<Tag> tagPage = tagMapper.selectPage(page, queryWrapper);
        return tagPage;
    }

    @Override
    public List<Tag> group() {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.groupBy("content");
        queryWrapper.having("count(*) > 10");
        return tagMapper.selectList(queryWrapper);
    }
}
