package com.caochenlei.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.caochenlei.domain.UserInfo;
import com.caochenlei.mapper.UserInfoMapper;
import com.caochenlei.service.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;
import java.util.Set;

@Service
@Transactional
public class UserInfoServiceImpl implements UserInfoService {
    @Autowired(required = false)
    private UserInfoMapper userInfoMapper;

    @Override
    public UserInfo insert(UserInfo userInfo) {
        userInfoMapper.insert(userInfo);
        return userInfo;
    }

    @Override
    public Integer deleteById(Integer id) {
        return userInfoMapper.deleteById(id);
    }

    @Override
    public Integer updateById(UserInfo userInfo) {
        return userInfoMapper.updateById(userInfo);
    }

    @Override
    public UserInfo selectById(Integer id) {
        return userInfoMapper.selectById(id);
    }

    @Override
    public Page<UserInfo> search(Map<String, Object> searchMap) {
        //定义查询参数
        QueryWrapper queryWrapper = new QueryWrapper();
        Integer pageNum = 1;
        Integer pageSize = 20;
        Map<String, String> orderEntity = null;
        Map<String, String> searchEntity = null;
        //接收查询参数
        if (searchMap.get("pageNum") != null) {
            pageNum = (Integer) searchMap.get("pageNum");
        }
        if (searchMap.get("pageSize") != null) {
            pageSize = (Integer) searchMap.get("pageSize");
        }
        if (searchMap.get("orderEntity") != null) {
            orderEntity = (Map<String, String>) searchMap.get("orderEntity");
            Set<Map.Entry<String, String>> OrderEntries = orderEntity.entrySet();
            for (Map.Entry<String, String> entry : OrderEntries) {
                String orderByColumn = entry.getKey();
                String orderByValue = entry.getValue();
                if ("asc".equals(orderByValue)) {
                    queryWrapper.orderByAsc(orderByColumn);
                } else {
                    queryWrapper.orderByDesc(orderByColumn);
                }
            }
        }
        if (searchMap.get("searchEntity") != null) {
            searchEntity = (Map<String, String>) searchMap.get("searchEntity");
            //查询主键列
            if (searchEntity.get("id") != null) {
                queryWrapper.eq("id", searchEntity.get("id"));
            }
            //查询普通列
            if (searchEntity.get("qq") != null) {
                queryWrapper.like("qq", searchEntity.get("qq"));
            }
            if (searchEntity.get("github") != null) {
                queryWrapper.like("github", searchEntity.get("github"));
            }
            if (searchEntity.get("gender") != null) {
                queryWrapper.like("gender", searchEntity.get("gender"));
            }
            if (searchEntity.get("sign") != null) {
                queryWrapper.like("sign", searchEntity.get("sign"));
            }
            if (searchEntity.get("wechat") != null) {
                queryWrapper.like("wechat", searchEntity.get("wechat"));
            }
            if (searchEntity.get("avatar") != null) {
                queryWrapper.like("avatar", searchEntity.get("avatar"));
            }
            if (searchEntity.get("gitee") != null) {
                queryWrapper.like("gitee", searchEntity.get("gitee"));
            }
            if (searchEntity.get("weibo") != null) {
                queryWrapper.like("weibo", searchEntity.get("weibo"));
            }
            if (searchEntity.get("phone") != null) {
                queryWrapper.like("phone", searchEntity.get("phone"));
            }
            if (searchEntity.get("nickname") != null) {
                queryWrapper.like("nickname", searchEntity.get("nickname"));
            }
            if (searchEntity.get("email") != null) {
                queryWrapper.like("email", searchEntity.get("email"));
            }
            if (searchEntity.get("username") != null) {
                queryWrapper.like("username", searchEntity.get("username"));
            }
        }
        //创建分页对象
        Page<UserInfo> page = new Page<>(pageNum, pageSize);
        //返回查询结果
        Page<UserInfo> userInfoPage = userInfoMapper.selectPage(page, queryWrapper);
        return userInfoPage;
    }
}
