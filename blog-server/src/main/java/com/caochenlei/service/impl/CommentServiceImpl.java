package com.caochenlei.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.caochenlei.domain.Comment;
import com.caochenlei.mapper.CommentMapper;
import com.caochenlei.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;
import java.util.Set;

@Service
@Transactional
public class CommentServiceImpl implements CommentService {
    @Autowired(required = false)
    private CommentMapper commentMapper;

    @Override
    public Comment insert(Comment comment) {
        commentMapper.insert(comment);
        return comment;
    }

    @Override
    public Integer deleteById(Integer id) {
        return commentMapper.deleteById(id);
    }

    @Override
    public Integer updateById(Comment comment) {
        return commentMapper.updateById(comment);
    }

    @Override
    public Comment selectById(Integer id) {
        return commentMapper.selectById(id);
    }

    @Override
    public Page<Comment> search(Map<String, Object> searchMap) {
        //定义查询参数
        QueryWrapper queryWrapper = new QueryWrapper();
        Integer pageNum = 1;
        Integer pageSize = 20;
        Map<String, String> orderEntity = null;
        Map<String, String> searchEntity = null;
        //接收查询参数
        if (searchMap.get("pageNum") != null) {
            pageNum = (Integer) searchMap.get("pageNum");
        }
        if (searchMap.get("pageSize") != null) {
            pageSize = (Integer) searchMap.get("pageSize");
        }
        if (searchMap.get("orderEntity") != null) {
            orderEntity = (Map<String, String>) searchMap.get("orderEntity");
            Set<Map.Entry<String, String>> OrderEntries = orderEntity.entrySet();
            for (Map.Entry<String, String> entry : OrderEntries) {
                String orderByColumn = entry.getKey();
                String orderByValue = entry.getValue();
                if ("asc".equals(orderByValue)) {
                    queryWrapper.orderByAsc(orderByColumn);
                } else {
                    queryWrapper.orderByDesc(orderByColumn);
                }
            }
        }
        if (searchMap.get("searchEntity") != null) {
            searchEntity = (Map<String, String>) searchMap.get("searchEntity");
            //查询主键列
            if (searchEntity.get("id") != null) {
                queryWrapper.eq("id", searchEntity.get("id"));
            }
            //查询普通列
            if (searchEntity.get("article_id") != null) {
                queryWrapper.like("article_id", searchEntity.get("article_id"));
            }
            if (searchEntity.get("create_time") != null) {
                queryWrapper.like("create_time", searchEntity.get("create_time"));
            }
            if (searchEntity.get("reviewer_name") != null) {
                queryWrapper.like("reviewer_name", searchEntity.get("reviewer_name"));
            }
            if (searchEntity.get("parent_id") != null) {
                queryWrapper.like("parent_id", searchEntity.get("parent_id"));
            }
            if (searchEntity.get("reviewer_email") != null) {
                queryWrapper.like("reviewer_email", searchEntity.get("reviewer_email"));
            }
            if (searchEntity.get("content") != null) {
                queryWrapper.like("content", searchEntity.get("content"));
            }
        }
        //创建分页对象
        Page<Comment> page = new Page<>(pageNum, pageSize);
        //返回查询结果
        Page<Comment> commentPage = commentMapper.selectPage(page, queryWrapper);
        return commentPage;
    }
}
