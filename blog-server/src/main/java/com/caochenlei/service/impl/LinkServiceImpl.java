package com.caochenlei.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.caochenlei.domain.Link;
import com.caochenlei.mapper.LinkMapper;
import com.caochenlei.service.LinkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;
import java.util.Set;

@Service
@Transactional
public class LinkServiceImpl implements LinkService {
    @Autowired(required = false)
    private LinkMapper linkMapper;

    @Override
    public Link insert(Link link) {
        linkMapper.insert(link);
        return link;
    }

    @Override
    public Integer deleteById(Integer id) {
        return linkMapper.deleteById(id);
    }

    @Override
    public Integer updateById(Link link) {
        return linkMapper.updateById(link);
    }

    @Override
    public Link selectById(Integer id) {
        return linkMapper.selectById(id);
    }

    @Override
    public Page<Link> search(Map<String, Object> searchMap) {
        //定义查询参数
        QueryWrapper queryWrapper = new QueryWrapper();
        Integer pageNum = 1;
        Integer pageSize = 20;
        Map<String, String> orderEntity = null;
        Map<String, String> searchEntity = null;
        //接收查询参数
        if (searchMap.get("pageNum") != null) {
            pageNum = (Integer) searchMap.get("pageNum");
        }
        if (searchMap.get("pageSize") != null) {
            pageSize = (Integer) searchMap.get("pageSize");
        }
        if (searchMap.get("orderEntity") != null) {
            orderEntity = (Map<String, String>) searchMap.get("orderEntity");
            Set<Map.Entry<String, String>> OrderEntries = orderEntity.entrySet();
            for (Map.Entry<String, String> entry : OrderEntries) {
                String orderByColumn = entry.getKey();
                String orderByValue = entry.getValue();
                if ("asc".equals(orderByValue)) {
                    queryWrapper.orderByAsc(orderByColumn);
                } else {
                    queryWrapper.orderByDesc(orderByColumn);
                }
            }
        }
        if (searchMap.get("searchEntity") != null) {
            searchEntity = (Map<String, String>) searchMap.get("searchEntity");
            //查询主键列
            if (searchEntity.get("id") != null) {
                queryWrapper.eq("id", searchEntity.get("id"));
            }
            //查询普通列
            if (searchEntity.get("cover") != null) {
                queryWrapper.like("cover", searchEntity.get("cover"));
            }
            if (searchEntity.get("name") != null) {
                queryWrapper.like("name", searchEntity.get("name"));
            }
            if (searchEntity.get("order_num") != null) {
                queryWrapper.like("order_num", searchEntity.get("order_num"));
            }
            if (searchEntity.get("addr") != null) {
                queryWrapper.like("addr", searchEntity.get("addr"));
            }
        }
        //创建分页对象
        Page<Link> page = new Page<>(pageNum, pageSize);
        //返回查询结果
        Page<Link> linkPage = linkMapper.selectPage(page, queryWrapper);
        return linkPage;
    }
}
