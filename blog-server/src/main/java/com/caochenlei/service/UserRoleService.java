package com.caochenlei.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.caochenlei.domain.UserRole;

import java.util.Map;

public interface UserRoleService {
    //插入一条记录
    public UserRole insert(UserRole userRole);

    //根据主键删除
    public Integer deleteById(Integer uid);

    //根据主键修改
    public Integer updateById(UserRole userRole);

    //根据主键查找
    public UserRole selectById(Integer uid);

    //根据条件查找
    public Page<UserRole> search(Map<String,Object> searchMap);
}
