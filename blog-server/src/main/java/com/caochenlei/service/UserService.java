package com.caochenlei.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.caochenlei.domain.User;

import java.util.Map;

public interface UserService {
    //插入一条记录
    public User insert(User user);

    //根据主键删除
    public Integer deleteById(Integer id);

    //根据主键修改
    public Integer updateById(User user);

    //根据主键查找
    public User selectById(Integer id);

    //根据条件查找
    public Page<User> search(Map<String,Object> searchMap);
}
