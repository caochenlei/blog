package com.caochenlei.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.caochenlei.domain.Carousel;

import java.util.Map;

public interface CarouselService {
    //插入一条记录
    public Carousel insert(Carousel carousel);

    //根据主键删除
    public Integer deleteById(Integer id);

    //根据主键修改
    public Integer updateById(Carousel carousel);

    //根据主键查找
    public Carousel selectById(Integer id);

    //根据条件查找
    public Page<Carousel> search(Map<String,Object> searchMap);
}
