package com.caochenlei.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.caochenlei.domain.UserInfo;

import java.util.Map;

public interface UserInfoService {
    //插入一条记录
    public UserInfo insert(UserInfo userInfo);

    //根据主键删除
    public Integer deleteById(Integer id);

    //根据主键修改
    public Integer updateById(UserInfo userInfo);

    //根据主键查找
    public UserInfo selectById(Integer id);

    //根据条件查找
    public Page<UserInfo> search(Map<String,Object> searchMap);
}
