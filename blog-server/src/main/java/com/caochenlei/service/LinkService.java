package com.caochenlei.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.caochenlei.domain.Link;

import java.util.Map;

public interface LinkService {
    //插入一条记录
    public Link insert(Link link);

    //根据主键删除
    public Integer deleteById(Integer id);

    //根据主键修改
    public Integer updateById(Link link);

    //根据主键查找
    public Link selectById(Integer id);

    //根据条件查找
    public Page<Link> search(Map<String,Object> searchMap);
}
