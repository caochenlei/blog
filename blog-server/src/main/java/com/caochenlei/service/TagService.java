package com.caochenlei.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.caochenlei.domain.Tag;

import java.util.List;
import java.util.Map;

public interface TagService {
    //插入一条记录
    public Tag insert(Tag tag);

    //根据主键删除
    public Integer deleteById(Integer id);

    //根据主键修改
    public Integer updateById(Tag tag);

    //根据主键查找
    public Tag selectById(Integer id);

    //根据条件查找
    public Page<Tag> search(Map<String,Object> searchMap);

    //查看热门标签
    List<Tag> group();
}
