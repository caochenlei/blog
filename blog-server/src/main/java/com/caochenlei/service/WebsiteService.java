package com.caochenlei.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.caochenlei.domain.Website;

import java.util.Map;

public interface WebsiteService {
    //插入一条记录
    public Website insert(Website website);

    //根据主键删除
    public Integer deleteById(Integer id);

    //根据主键修改
    public Integer updateById(Website website);

    //根据主键查找
    public Website selectById(Integer id);

    //根据条件查找
    public Page<Website> search(Map<String,Object> searchMap);
}
