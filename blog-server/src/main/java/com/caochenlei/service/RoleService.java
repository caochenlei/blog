package com.caochenlei.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.caochenlei.domain.Role;

import java.util.Map;

public interface RoleService {
    //插入一条记录
    public Role insert(Role role);

    //根据主键删除
    public Integer deleteById(Integer id);

    //根据主键修改
    public Integer updateById(Role role);

    //根据主键查找
    public Role selectById(Integer id);

    //根据条件查找
    public Page<Role> search(Map<String,Object> searchMap);
}
