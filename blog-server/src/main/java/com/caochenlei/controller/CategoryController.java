package com.caochenlei.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.caochenlei.domain.Category;
import com.caochenlei.domain.Result;
import com.caochenlei.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@Api(tags = "文章分类接口")
public class CategoryController {
    @Autowired
    private CategoryService categoryService;

    //插入一条记录
    @PostMapping("/categorys")
    @ApiOperation(value = "插入一条记录")
    @Secured({"ROLE_ADMIN"})
    public Result insert(@RequestBody Category category) {
        category = categoryService.insert(category);
        if (category.getId() != null) {
            return new Result(true, category, "操作成功");
        } else {
            return new Result(false, null, "操作失败");
        }
    }

    //根据主键删除
    @DeleteMapping("/categorys/{id}")
    @ApiOperation(value = "根据主键删除")
    @Secured({"ROLE_ADMIN"})
    public Result deleteById(@PathVariable("id") Integer id) {
        Integer rows = categoryService.deleteById(id);
        if (rows > 0) {
            return new Result(true, rows, "操作成功");
        } else {
            return new Result(false, rows, "操作失败");
        }
    }

    //根据主键修改
    @PutMapping("/categorys")
    @ApiOperation(value = "根据主键修改")
    @Secured({"ROLE_ADMIN"})
    public Result updateById(@RequestBody Category category) {
        Integer rows = categoryService.updateById(category);
        if (rows > 0) {
            return new Result(true, rows, "操作成功");
        } else {
            return new Result(false, rows, "操作失败");
        }
    }

    //根据主键查找
    @GetMapping("/common/categorys/{id}")
    @ApiOperation(value = "根据主键查找")
    public Result selectById(@PathVariable("id") Integer id) {
        Category category = categoryService.selectById(id);
        if (category != null) {
            return new Result(true, category, "操作成功");
        } else {
            return new Result(false, null, "操作失败");
        }
    }

    //根据条件查找
    @PostMapping("/common/categorys/page")
    @ApiOperation(value = "根据条件查找")
    public Result search(@RequestBody Map<String, Object> searchMap) {
        Page<Category> categoryPage = categoryService.search(searchMap);
        if (categoryPage != null) {
            return new Result(true, categoryPage, "操作成功");
        } else {
            return new Result(false, null, "操作失败");
        }
    }
}
