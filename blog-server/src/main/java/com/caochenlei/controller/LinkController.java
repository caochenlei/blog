package com.caochenlei.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.caochenlei.domain.Link;
import com.caochenlei.domain.Result;
import com.caochenlei.service.LinkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@Api(tags = "友情链接接口")
public class LinkController {
    @Autowired
    private LinkService linkService;

    //插入一条记录
    @PostMapping("/links")
    @ApiOperation(value = "插入一条记录")
    @Secured({"ROLE_ADMIN"})
    public Result insert(@RequestBody Link link) {
        link = linkService.insert(link);
        if (link.getId() != null) {
            return new Result(true, link, "操作成功");
        } else {
            return new Result(false, null, "操作失败");
        }
    }

    //根据主键删除
    @DeleteMapping("/links/{id}")
    @ApiOperation(value = "根据主键删除")
    @Secured({"ROLE_ADMIN"})
    public Result deleteById(@PathVariable("id") Integer id) {
        Integer rows = linkService.deleteById(id);
        if (rows > 0) {
            return new Result(true, rows, "操作成功");
        } else {
            return new Result(false, rows, "操作失败");
        }
    }

    //根据主键修改
    @PutMapping("/links")
    @ApiOperation(value = "根据主键修改")
    @Secured({"ROLE_ADMIN"})
    public Result updateById(@RequestBody Link link) {
        Integer rows = linkService.updateById(link);
        if (rows > 0) {
            return new Result(true, rows, "操作成功");
        } else {
            return new Result(false, rows, "操作失败");
        }
    }

    //根据主键查找
    @GetMapping("/common/links/{id}")
    @ApiOperation(value = "根据主键查找")
    public Result selectById(@PathVariable("id") Integer id) {
        Link link = linkService.selectById(id);
        if (link != null) {
            return new Result(true, link, "操作成功");
        } else {
            return new Result(false, null, "操作失败");
        }
    }

    //根据条件查找
    @PostMapping("/common/links/page")
    @ApiOperation(value = "根据条件查找")
    public Result search(@RequestBody Map<String, Object> searchMap) {
        Page<Link> linkPage = linkService.search(searchMap);
        if (linkPage != null) {
            return new Result(true, linkPage, "操作成功");
        } else {
            return new Result(false, null, "操作失败");
        }
    }
}
