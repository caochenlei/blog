package com.caochenlei.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.caochenlei.domain.Result;
import com.caochenlei.domain.User;
import com.caochenlei.domain.UserInfo;
import com.caochenlei.domain.UserVo;
import com.caochenlei.service.UserInfoService;
import com.caochenlei.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@Api(tags = "用户账户接口")
public class UserController {
    @Autowired
    private UserService userService;
    @Autowired
    private UserInfoService userInfoService;

    //插入一条记录
    @PostMapping("/users")
    @ApiOperation(value = "插入一条记录")
    @Secured({"ROLE_ADMIN"})
    public Result insert(@RequestBody User user) {
        user = userService.insert(user);
        if (user.getId() != null) {
            return new Result(true, user, "操作成功");
        } else {
            return new Result(false, null, "操作失败");
        }
    }

    //根据主键删除
    @DeleteMapping("/users/{id}")
    @ApiOperation(value = "根据主键删除")
    @Secured({"ROLE_ADMIN"})
    public Result deleteById(@PathVariable("id") Integer id) {
        Integer rows = userService.deleteById(id);
        if (rows > 0) {
            return new Result(true, rows, "操作成功");
        } else {
            return new Result(false, rows, "操作失败");
        }
    }

    //根据主键修改
    @PutMapping("/users")
    @ApiOperation(value = "根据主键修改")
    @Secured({"ROLE_ADMIN"})
    public Result updateById(@RequestBody User user) {
        Integer rows = userService.updateById(user);
        if (rows > 0) {
            return new Result(true, rows, "操作成功");
        } else {
            return new Result(false, rows, "操作失败");
        }
    }

    //根据主键查找
    @GetMapping("/common/users/{id}")
    @ApiOperation(value = "根据主键查找")
    public Result selectById(@PathVariable("id") Integer id) {
        User user = userService.selectById(id);
        if (user != null) {
            return new Result(true, user, "操作成功");
        } else {
            return new Result(false, null, "操作失败");
        }
    }

    //根据条件查找
    @PostMapping("/common/users/page")
    @ApiOperation(value = "根据条件查找")
    public Result search(@RequestBody Map<String, Object> searchMap) {
        Page<User> userPage = userService.search(searchMap);
        if (userPage != null) {
            return new Result(true, userPage, "操作成功");
        } else {
            return new Result(false, null, "操作失败");
        }
    }

    //根据名称获取
    @PostMapping("/common/users/findByUsername")
    @ApiOperation(value = "根据名称获取")
    public Result findByUsername(@RequestParam("username") String username) {
        Map<String, Object> searchMap = new HashMap<>();
        HashMap<String, String> search = new HashMap<>();
        search.put("username", username);
        searchMap.put("searchEntity", search);
        Page<User> userPage = userService.search(searchMap);
        Page<UserInfo> userInfoPage = userInfoService.search(searchMap);
        if (userPage != null && userInfoPage != null) {
            UserVo userVo = new UserVo();
            List<User> userPageRecords = userPage.getRecords();
            if (userPageRecords != null) {
                userVo.setUser(userPageRecords.get(0));
            }
            List<UserInfo> userInfoPageRecords = userInfoPage.getRecords();
            if (userInfoPageRecords != null) {
                userVo.setUserInfo(userInfoPageRecords.get(0));
            }
            return new Result(true, userVo, "操作成功");
        } else {
            return new Result(false, null, "操作失败");
        }
    }
}
