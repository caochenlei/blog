package com.caochenlei.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.caochenlei.domain.UserInfo;
import com.caochenlei.domain.Result;
import com.caochenlei.service.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@Api(tags = "用户信息接口")
public class UserInfoController {
    @Autowired
    private UserInfoService userInfoService;

    //插入一条记录
    @PostMapping("/userInfos")
    @ApiOperation(value = "插入一条记录")
    @Secured({"ROLE_ADMIN"})
    public Result insert(@RequestBody UserInfo userInfo) {
        userInfo = userInfoService.insert(userInfo);
        if (userInfo.getId() != null) {
            return new Result(true, userInfo, "操作成功");
        } else {
            return new Result(false, null, "操作失败");
        }
    }

    //根据主键删除
    @DeleteMapping("/userInfos/{id}")
    @ApiOperation(value = "根据主键删除")
    @Secured({"ROLE_ADMIN"})
    public Result deleteById(@PathVariable("id") Integer id) {
        Integer rows = userInfoService.deleteById(id);
        if (rows > 0) {
            return new Result(true, rows, "操作成功");
        } else {
            return new Result(false, rows, "操作失败");
        }
    }

    //根据主键修改
    @PutMapping("/userInfos")
    @ApiOperation(value = "根据主键修改")
    @Secured({"ROLE_ADMIN"})
    public Result updateById(@RequestBody UserInfo userInfo) {
        Integer rows = userInfoService.updateById(userInfo);
        if (rows > 0) {
            return new Result(true, rows, "操作成功");
        } else {
            return new Result(false, rows, "操作失败");
        }
    }

    //根据主键查找
    @GetMapping("/common/userInfos/{id}")
    @ApiOperation(value = "根据主键查找")
    public Result selectById(@PathVariable("id") Integer id) {
        UserInfo userInfo = userInfoService.selectById(id);
        if (userInfo != null) {
            return new Result(true, userInfo, "操作成功");
        } else {
            return new Result(false, null, "操作失败");
        }
    }

    //根据条件查找
    @PostMapping("/common/userInfos/page")
    @ApiOperation(value = "根据条件查找")
    public Result search(@RequestBody Map<String, Object> searchMap) {
        Page<UserInfo> userInfoPage = userInfoService.search(searchMap);
        if (userInfoPage != null) {
            return new Result(true, userInfoPage, "操作成功");
        } else {
            return new Result(false, null, "操作失败");
        }
    }
}
