package com.caochenlei.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.caochenlei.domain.Comment;
import com.caochenlei.domain.Result;
import com.caochenlei.service.CommentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@Api(tags = "文章评论接口")
public class CommentController {
    @Autowired
    private CommentService commentService;

    //插入一条记录
    @PostMapping("/common/comments")
    @ApiOperation(value = "插入一条记录")
//    @Secured({"ROLE_ADMIN"})
    public Result insert(@RequestBody Comment comment) {
        comment = commentService.insert(comment);
        if (comment.getId() != null) {
            return new Result(true, comment, "操作成功");
        } else {
            return new Result(false, null, "操作失败");
        }
    }

    //根据主键删除
    @DeleteMapping("/comments/{id}")
    @ApiOperation(value = "根据主键删除")
    @Secured({"ROLE_ADMIN"})
    public Result deleteById(@PathVariable("id") Integer id) {
        Integer rows = commentService.deleteById(id);
        if (rows > 0) {
            return new Result(true, rows, "操作成功");
        } else {
            return new Result(false, rows, "操作失败");
        }
    }

    //根据主键修改
    @PutMapping("/comments")
    @ApiOperation(value = "根据主键修改")
    @Secured({"ROLE_ADMIN"})
    public Result updateById(@RequestBody Comment comment) {
        Integer rows = commentService.updateById(comment);
        if (rows > 0) {
            return new Result(true, rows, "操作成功");
        } else {
            return new Result(false, rows, "操作失败");
        }
    }

    //根据主键查找
    @GetMapping("/common/comments/{id}")
    @ApiOperation(value = "根据主键查找")
    public Result selectById(@PathVariable("id") Integer id) {
        Comment comment = commentService.selectById(id);
        if (comment != null) {
            return new Result(true, comment, "操作成功");
        } else {
            return new Result(false, null, "操作失败");
        }
    }

    //根据条件查找
    @PostMapping("/common/comments/page")
    @ApiOperation(value = "根据条件查找")
    public Result search(@RequestBody Map<String, Object> searchMap) {
        Page<Comment> commentPage = commentService.search(searchMap);
        if (commentPage != null) {
            return new Result(true, commentPage, "操作成功");
        } else {
            return new Result(false, null, "操作失败");
        }
    }

    //获取文章评论
    @PostMapping("/common/comments/findAllCommentsByArticleId")
    @ApiOperation(value = "获取文章评论", notes = "返回值中，键代表父评论id，值代表该评论下所有的回复，默认层级只有两级")
    public Result findAllCommentsByArticleId(@RequestParam("articleId") Integer articleId) {
        List<Comment> allComments = new ArrayList<>();
        Map<String, Object> searchMap = new HashMap<>();

        HashMap<String, Object> searchEntity = new HashMap<>();
        searchEntity.put("article_id", articleId);
        searchMap.put("searchEntity", searchEntity);

        HashMap<String, Object> orderEntity = new HashMap<>();
        orderEntity.put("parent_id", "asc");
        searchMap.put("orderEntity", orderEntity);

        Page<Comment> commentPage = commentService.search(searchMap);
        if (commentPage != null) {
            List<Comment> commentPageRecords = commentPage.getRecords();
            if (commentPageRecords != null) {
                for (Comment comment : commentPageRecords) {
                    //说明是第一层评论
                    if (comment.getParentId() == null) {
                        comment.setReply(new ArrayList<>());
                        allComments.add(comment);
                    }
                }
                for (Comment comment : commentPageRecords) {
                    //说明是第二层评论
                    if (comment.getParentId() != null) {
                        for (int i = 0; i < allComments.size(); i++) {
                            if (allComments.get(i).getId().intValue() == comment.getParentId().intValue()) {
                                allComments.get(i).getReply().add(comment);
                                break;
                            }
                        }
                    }
                }
            }
            return new Result(true, allComments, "操作成功");
        } else {
            return new Result(false, null, "操作失败");
        }
    }
}
