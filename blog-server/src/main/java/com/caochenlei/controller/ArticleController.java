package com.caochenlei.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.caochenlei.domain.Article;
import com.caochenlei.domain.ArticleVo;
import com.caochenlei.domain.Comment;
import com.caochenlei.domain.Result;
import com.caochenlei.service.ArticleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@Api(tags = "文章信息接口")
public class ArticleController {
    @Autowired
    private ArticleService articleService;
    @Autowired
    private CommentController commentController;

    //插入一条记录
    @PostMapping("/articles")
    @ApiOperation(value = "插入一条记录", notes = "上传文章信息，需要传入文章信息和标签列表，不用传评论列表")
    @Secured({"ROLE_ADMIN"})
    public Result insert(@RequestBody ArticleVo articleVo) {
        articleVo = articleService.insert(articleVo);
        if (articleVo != null && articleVo.getArticle().getId() != null) {
            return new Result(true, articleVo, "操作成功");
        } else {
            return new Result(false, null, "操作失败");
        }
    }

    //根据主键删除
    @DeleteMapping("/articles/{id}")
    @ApiOperation(value = "根据主键删除", notes = "删除文章信息、标签列表、评论列表三部分内容")
    @Secured({"ROLE_ADMIN"})
    public Result deleteById(@PathVariable("id") Integer id) {
        Integer rows = articleService.deleteById(id);
        if (rows > 0) {
            return new Result(true, rows, "操作成功");
        } else {
            return new Result(false, rows, "操作失败");
        }
    }

    //根据主键修改
    @PutMapping("/articles")
    @ApiOperation(value = "根据主键修改", notes = "修改文章信息，需要传入文章信息和标签列表，不用传评论列表")
    @Secured({"ROLE_ADMIN"})
    public Result updateById(@RequestBody ArticleVo articleVo) {
        Integer rows = articleService.updateById(articleVo);
        if (rows > 0) {
            return new Result(true, rows, "操作成功");
        } else {
            return new Result(false, rows, "操作失败");
        }
    }

    //根据主键查找
    @GetMapping("/common/articles/{id}")
    @ApiOperation(value = "根据主键查找", notes = "返回文章信息、标签列表，但是不包含评论列表")
    public Result selectById(@PathVariable("id") Integer id) {
        ArticleVo articleVo = articleService.selectById(id);
        if (articleVo != null) {
            return new Result(true, articleVo, "操作成功");
        } else {
            return new Result(false, null, "操作失败");
        }
    }

    //根据条件查找
    @PostMapping("/common/articles/page")
    @ApiOperation(value = "根据条件查找", notes = "返回文章信息、但是不包含标签列表、评论列表、文章内容")
    public Result search(@RequestBody Map<String, Object> searchMap) {
        Page<ArticleVo> articleVoPage = articleService.search(searchMap);
        if (articleVoPage != null) {
            return new Result(true, articleVoPage, "操作成功");
        } else {
            return new Result(false, null, "操作失败");
        }
    }

    //获取文章详情
    @GetMapping("/common/articles/details/{id}")
    @ApiOperation(value = "获取文章详情", notes = "返回文章信息、标签列表、评论列表三部分内容")
    public Result selectDetailsById(@PathVariable("id") Integer id) {
        ArticleVo articleVo = articleService.selectById(id);
        Result allCommentsByArticleId = commentController.findAllCommentsByArticleId(id);
        if (allCommentsByArticleId.getData() != null) {
            List<Comment> comments = (List<Comment>) allCommentsByArticleId.getData();
            articleVo.setComments(comments);
        }
        if (articleVo != null) {
            return new Result(true, articleVo, "操作成功");
        } else {
            return new Result(false, null, "操作失败");
        }
    }

    //更新数量状态
    @PutMapping("/common/articles/count")
    @ApiOperation(value = "更新数量状态", notes = "变更阅读数量（read）、赞👍数量（support）、踩👎数量（oppose）")
    public Result updateCount(
            @RequestParam("articleId") Integer articleId,
            @RequestParam("filedName") String filedName,
            @RequestParam("count") Long count) {
        Integer rows = articleService.updateCount(articleId, filedName, count);
        if (rows > 0) {
            return new Result(true, rows, "操作成功");
        } else {
            return new Result(false, rows, "操作失败");
        }
    }
}
