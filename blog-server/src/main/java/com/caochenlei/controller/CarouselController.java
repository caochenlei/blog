package com.caochenlei.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.caochenlei.domain.Carousel;
import com.caochenlei.domain.Result;
import com.caochenlei.service.CarouselService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@Api(tags = "轮播信息接口")
public class CarouselController {
    @Autowired
    private CarouselService carouselService;

    //插入一条记录
    @PostMapping("/carousels")
    @ApiOperation(value = "插入一条记录")
    @Secured({"ROLE_ADMIN"})
    public Result insert(@RequestBody Carousel carousel) {
        carousel = carouselService.insert(carousel);
        if (carousel.getId() != null) {
            return new Result(true, carousel, "操作成功");
        } else {
            return new Result(false, null, "操作失败");
        }
    }

    //根据主键删除
    @DeleteMapping("/carousels/{id}")
    @ApiOperation(value = "根据主键删除")
    @Secured({"ROLE_ADMIN"})
    public Result deleteById(@PathVariable("id") Integer id) {
        Integer rows = carouselService.deleteById(id);
        if (rows > 0) {
            return new Result(true, rows, "操作成功");
        } else {
            return new Result(false, rows, "操作失败");
        }
    }

    //根据主键修改
    @PutMapping("/carousels")
    @ApiOperation(value = "根据主键修改")
    @Secured({"ROLE_ADMIN"})
    public Result updateById(@RequestBody Carousel carousel) {
        Integer rows = carouselService.updateById(carousel);
        if (rows > 0) {
            return new Result(true, rows, "操作成功");
        } else {
            return new Result(false, rows, "操作失败");
        }
    }

    //根据主键查找
    @GetMapping("/common/carousels/{id}")
    @ApiOperation(value = "根据主键查找")
    public Result selectById(@PathVariable("id") Integer id) {
        Carousel carousel = carouselService.selectById(id);
        if (carousel != null) {
            return new Result(true, carousel, "操作成功");
        } else {
            return new Result(false, null, "操作失败");
        }
    }

    //根据条件查找
    @PostMapping("/common/carousels/page")
    @ApiOperation(value = "根据条件查找")
    public Result search(@RequestBody Map<String, Object> searchMap) {
        Page<Carousel> carouselPage = carouselService.search(searchMap);
        if (carouselPage != null) {
            return new Result(true, carouselPage, "操作成功");
        } else {
            return new Result(false, null, "操作失败");
        }
    }
}
