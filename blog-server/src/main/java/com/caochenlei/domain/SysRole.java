package com.caochenlei.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SysRole implements GrantedAuthority {
    private Integer id;
    private String name;
    private String desc;

    @JsonIgnore
    @Override
    public String getAuthority() {
        return name;
    }
}