package com.caochenlei.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("art_comment")
@ApiModel(value = "文章评论类型")
public class Comment {
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(name = "`id`", value = "评论id", required = true)
    private Integer id;
    @TableField("`article_id`")
    @ApiModelProperty(name = "article_id", value = "文章id", required = true)
    private Integer articleId;
    @TableField("`create_time`")
    @ApiModelProperty(name = "create_time", value = "评论时间", required = true)
    private java.util.Date createTime;
    @TableField("`reviewer_name`")
    @ApiModelProperty(name = "reviewer_name", value = "评论人名称", required = true)
    private String reviewerName;
    @TableField("`parent_id`")
    @ApiModelProperty(name = "parent_id", value = "父评论id", required = true)
    private Integer parentId;
    @TableField("`reviewer_email`")
    @ApiModelProperty(name = "reviewer_email", value = "评论人邮箱", required = true)
    private String reviewerEmail;
    @TableField("`content`")
    @ApiModelProperty(name = "content", value = "评论内容", required = true)
    private String content;

    @TableField(exist = false)
    private List<Comment> reply;
}
