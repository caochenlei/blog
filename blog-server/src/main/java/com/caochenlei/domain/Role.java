package com.caochenlei.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("sys_role")
@ApiModel(value = "用户角色类型")
public class Role {
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(name = "`id`", value = "角色id", required = true)
    private Integer id;
    @TableField("`name`")
    @ApiModelProperty(name = "name", value = "角色名称", required = true)
    private String name;
    @TableField("`desc`")
    @ApiModelProperty(name = "desc", value = "角色描述", required = true)
    private String desc;
}
