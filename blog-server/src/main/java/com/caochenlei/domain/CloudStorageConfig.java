package com.caochenlei.domain;


import lombok.Data;

/**
 * 云存储配置信息
 */
@Data
public class CloudStorageConfig {
    //腾讯云绑定的域名
    private String qcloudDomain="";
    //腾讯云路径前缀
    private String qcloudPrefix="";
    //腾讯云AppId
    private Integer qcloudAppId;
    //腾讯云SecretId
    private String qcloudSecretId="";
    //腾讯云SecretKey
    private String qcloudSecretKey="";
    //腾讯云BucketName
    private String qcloudBucketName="";
    //腾讯云COS所属地区
    private String qcloudRegion="";
}
