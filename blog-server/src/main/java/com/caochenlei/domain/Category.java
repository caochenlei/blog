package com.caochenlei.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("art_category")
@ApiModel(value = "文章分类类型")
public class Category {
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(name = "`id`", value = "分类id", required = true)
    private Integer id;
    @TableField("`name`")
    @ApiModelProperty(name = "name", value = "分类名称", required = true)
    private String name;
    @TableField("`icon`")
    @ApiModelProperty(name = "icon", value = "分类图标", required = true)
    private String icon;
    @TableField("`order_num`")
    @ApiModelProperty(name = "order_num", value = "分类顺序", required = true)
    private Integer orderNum;
}
