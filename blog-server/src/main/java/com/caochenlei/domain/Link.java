package com.caochenlei.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("sys_link")
@ApiModel(value = "友情链接类型")
public class Link {
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(name = "`id`", value = "友情链接id", required = true)
    private Integer id;
    @TableField("`cover`")
    @ApiModelProperty(name = "cover", value = "友情链接封面", required = true)
    private String cover;
    @TableField("`name`")
    @ApiModelProperty(name = "name", value = "友情链接名称", required = true)
    private String name;
    @TableField("`order_num`")
    @ApiModelProperty(name = "order_num", value = "友情链接顺序", required = true)
    private Integer orderNum;
    @TableField("`addr`")
    @ApiModelProperty(name = "addr", value = "友情链接地址", required = true)
    private String addr;
}
