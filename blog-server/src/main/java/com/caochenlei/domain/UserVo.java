package com.caochenlei.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "用户详细信息")
public class UserVo {
    @ApiModelProperty(name = "user", value = "用户基本信息", required = true)
    private User user;
    @ApiModelProperty(name = "userInfo", value = "用户拓展信息", required = true)
    private UserInfo userInfo;
}
