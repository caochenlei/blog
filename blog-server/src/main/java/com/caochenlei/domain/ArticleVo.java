package com.caochenlei.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "上传文章信息")
public class ArticleVo {
    @ApiModelProperty(name = "article", value = "文章信息", required = true)
    private Article article;
    @ApiModelProperty(name = "tags", value = "文章标签列表", required = false)
    private List<Tag> tags;
    @ApiModelProperty(name = "comments", value = "文章评论列表", required = false, hidden = true)
    private List<Comment> comments;
    @ApiModelProperty(name = "category", value = "文章分类名称", required = false, hidden = true)
    private Category category;
}
