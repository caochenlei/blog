package com.caochenlei.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("art_article")
@ApiModel(value = "文章信息类型")
public class Article {
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(name = "`id`", value = "文章id", required = true)
    private Integer id;
    @TableField("`summary`")
    @ApiModelProperty(name = "summary", value = "文章摘要", required = true)
    private String summary;
    @TableField("`read`")
    @ApiModelProperty(name = "read", value = "文章阅读量", required = true)
    private Long read;
    @TableField("`is_original`")
    @ApiModelProperty(name = "is_original", value = "是否原创（0：原创、1：转载）", required = true)
    private Integer isOriginal;
    @TableField("`create_time`")
    @ApiModelProperty(name = "create_time", value = "创建日期", required = true)
    private java.util.Date createTime;
    @TableField("`title`")
    @ApiModelProperty(name = "title", value = "文章标题", required = true)
    private String title;
    @TableField("`content`")
    @ApiModelProperty(name = "content", value = "文章内容", required = true)
    private String content;
    @TableField("`is_top`")
    @ApiModelProperty(name = "is_top", value = "是否置顶（0：置顶、1：取消）", required = true)
    private Integer isTop;
    @TableField("`cover`")
    @ApiModelProperty(name = "cover", value = "文章封面", required = true)
    private String cover;
    @TableField("`update_time`")
    @ApiModelProperty(name = "update_time", value = "修改日期", required = true)
    private java.util.Date updateTime;
    @TableField("`category_id`")
    @ApiModelProperty(name = "category_id", value = "分类id", required = true)
    private Integer categoryId;
    @TableField("`oppose`")
    @ApiModelProperty(name = "oppose", value = "文章反对踩", required = true)
    private Long oppose;
    @TableField("`support`")
    @ApiModelProperty(name = "support", value = "文章支持赞", required = true)
    private Long support;
}
