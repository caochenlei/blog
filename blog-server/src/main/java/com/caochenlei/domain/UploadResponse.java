package com.caochenlei.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor

@ApiModel(value = "上传响应对象")
public class UploadResponse {
    @ApiModelProperty(name = "fileName", value = "上传后文件名称", required = true)
    private String fileName;
    @ApiModelProperty(name = "originalFileName", value = "上传文件原名称", required = true)
    private String originalFileName;
    @ApiModelProperty(name = "size", value = "上传文件的大小", required = true)
    private Long size;
    @ApiModelProperty(name = "type", value = "上传文件的类型", required = true)
    private String type;
    @ApiModelProperty(name = "url", value = "上传后文件路径", required = true)
    private String url;
    @ApiModelProperty(name = "status", value = "本次上传的状态", required = true)
    private Integer status;
    @ApiModelProperty(name = "msg", value = "本次上传的提示", required = true)
    private String msg;

    public UploadResponse(String originalFileName, Integer status, String msg) {
        this.originalFileName = originalFileName;
        this.status = status;
        this.msg = msg;
    }
}
