package com.caochenlei.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("sys_user")
@ApiModel(value = "用户账户类型")
public class User {
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(name = "`id`", value = "用户id", required = true)
    private Integer id;
    @TableField("`password`")
    @ApiModelProperty(name = "password", value = "用户密码", required = true)
    private String password;
    @TableField("`username`")
    @ApiModelProperty(name = "username", value = "用户名称", required = true)
    private String username;
    @TableField("`status`")
    @ApiModelProperty(name = "status", value = "用户状态（0：关闭、1：开启）", required = true)
    private Integer status;
}
