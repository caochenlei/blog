package com.caochenlei.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("sys_carousel")
@ApiModel(value = "轮播信息类型")
public class Carousel {
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(name = "`id`", value = "轮播图id", required = true)
    private Integer id;
    @TableField("`cover`")
    @ApiModelProperty(name = "cover", value = "轮播图封面", required = true)
    private String cover;
    @TableField("`name`")
    @ApiModelProperty(name = "name", value = "轮播图名称", required = true)
    private String name;
    @TableField("`order_num`")
    @ApiModelProperty(name = "order_num", value = "轮播图顺序", required = true)
    private Integer orderNum;
    @TableField("`addr`")
    @ApiModelProperty(name = "addr", value = "轮播图地址", required = true)
    private String addr;
}
