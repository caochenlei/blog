package com.caochenlei.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("sys_user_info")
@ApiModel(value = "用户信息类型")
public class UserInfo {
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(name = "`id`", value = "用户信息id", required = true)
    private Integer id;
    @TableField("`qq`")
    @ApiModelProperty(name = "qq", value = "用户QQ", required = true)
    private String qq;
    @TableField("`github`")
    @ApiModelProperty(name = "github", value = "用户Github", required = true)
    private String github;
    @TableField("`gender`")
    @ApiModelProperty(name = "gender", value = "用户性别（0：男、1：女）", required = true)
    private Integer gender;
    @TableField("`sign`")
    @ApiModelProperty(name = "sign", value = "用户签名", required = true)
    private String sign;
    @TableField("`wechat`")
    @ApiModelProperty(name = "wechat", value = "用户微信", required = true)
    private String wechat;
    @TableField("`avatar`")
    @ApiModelProperty(name = "avatar", value = "用户头像", required = true)
    private String avatar;
    @TableField("`gitee`")
    @ApiModelProperty(name = "gitee", value = "用户Gitee", required = true)
    private String gitee;
    @TableField("`weibo`")
    @ApiModelProperty(name = "weibo", value = "用户微博", required = true)
    private String weibo;
    @TableField("`phone`")
    @ApiModelProperty(name = "phone", value = "用户手机", required = true)
    private String phone;
    @TableField("`nickname`")
    @ApiModelProperty(name = "nickname", value = "用户昵称", required = true)
    private String nickname;
    @TableField("`email`")
    @ApiModelProperty(name = "email", value = "用户邮箱", required = true)
    private String email;
    @TableField("`username`")
    @ApiModelProperty(name = "username", value = "用户名称", required = true)
    private String username;
}
