package com.caochenlei.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("sys_website")
@ApiModel(value = "网站信息类型")
public class Website {
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(name = "`id`", value = "网站id", required = true)
    private Integer id;
    @TableField("`copyright`")
    @ApiModelProperty(name = "copyright", value = "网站版权", required = true)
    private String copyright;
    @TableField("`name`")
    @ApiModelProperty(name = "name", value = "网站名称", required = true)
    private String name;
    @TableField("`logo`")
    @ApiModelProperty(name = "logo", value = "网站LOGO", required = true)
    private String logo;
}
