package com.caochenlei.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("art_tag")
@ApiModel(value = "文章标签类型")
public class Tag {
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(name = "`id`", value = "标签id", required = true)
    private Integer id;
    @TableField("`article_id`")
    @ApiModelProperty(name = "article_id", value = "文章id", required = true)
    private Integer articleId;
    @TableField("`content`")
    @ApiModelProperty(name = "content", value = "标签内容", required = true)
    private String content;
}
