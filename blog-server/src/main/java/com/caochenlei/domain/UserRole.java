package com.caochenlei.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("sys_user_role")
@ApiModel(value = "用户与角色关联表类型")
public class UserRole {
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(name = "`uid`", value = "用户id", required = true)
    private Integer uid;
}
