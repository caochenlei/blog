# 项目简介

Blog是一款个人博客管理系统，是我和同学上学期的期末大作业，完成的比较仓促，大部分功能已经完成。

访问地址：[https://gitee.com/caochenlei/blog](https://gitee.com/caochenlei/blog)

# 主要页面

- ### 网站前台

![](https://img-blog.csdnimg.cn/6b67e85bd3444f3f8d8b6dc3d683674b.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM4NDkwNDU3,size_16,color_FFFFFF,t_70#pic_center)

- ### 分类专栏

![](https://img-blog.csdnimg.cn/db0dc42aedb6417e865110f679c16747.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM4NDkwNDU3,size_16,color_FFFFFF,t_70#pic_center)

- ### 搜索模块

![](https://img-blog.csdnimg.cn/6f790ad605384c06b9e925d629960219.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM4NDkwNDU3,size_16,color_FFFFFF,t_70#pic_center)

- ### 文章详情

![](https://img-blog.csdnimg.cn/c6ca4d803d144203b2f1f09752ee5f42.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM4NDkwNDU3,size_16,color_FFFFFF,t_70#pic_center)

- ### 网站后台

![](https://img-blog.csdnimg.cn/a7bd67c33b7143a780842e23f8efffaf.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM4NDkwNDU3,size_16,color_FFFFFF,t_70#pic_center)

- ### 分类管理

![](https://img-blog.csdnimg.cn/6cb090ab88f943a2b917c0076642afa5.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM4NDkwNDU3,size_16,color_FFFFFF,t_70#pic_center)

- ### 文章添加

![](https://img-blog.csdnimg.cn/e25504146a8f4efcaa0fa65b04b3d93d.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM4NDkwNDU3,size_16,color_FFFFFF,t_70#pic_center)

- ### 文章列表

![](https://img-blog.csdnimg.cn/638e5cfad31e41f2b2e100860120075b.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM4NDkwNDU3,size_16,color_FFFFFF,t_70#pic_center)

- ### 轮播管理

![](https://img-blog.csdnimg.cn/31f56243707d4b34a75f054c3235fd79.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM4NDkwNDU3,size_16,color_FFFFFF,t_70#pic_center)

- ### 博主信息

![](https://img-blog.csdnimg.cn/d761c37e871e438796c86dbbc6c54059.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM4NDkwNDU3,size_16,color_FFFFFF,t_70#pic_center)

- ### 网站信息

![](https://img-blog.csdnimg.cn/698dbf35a4e84fc18f970c914ea8bd0c.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM4NDkwNDU3,size_16,color_FFFFFF,t_70#pic_center)

# 采用技术

**前端技术栈：**

| 技术名称 | 技术版本 | 官方地址                                 |
| -------- | -------- | ---------------------------------------- |
| React    | 17.0.2   | [点击访问](https://react.docschina.org/) |
| Antd     | 4.15.2   | [点击访问](https://ant.design/index-cn)  |

**后端技术栈：**

| 技术名称        | 技术版本 | 官方地址                                                     |
| --------------- | -------- | ------------------------------------------------------------ |
| Spring Boot     | 2.4.5    | [点击访问](https://docs.spring.io/spring-boot/docs/2.4.5/reference/html/) |
| Spring Security | 5.4.6    | [点击访问](https://docs.spring.io/spring-security/site/docs/5.4.6/reference/html5/) |
| Tomcat          | 9.0.45   | [点击访问](https://tomcat.apache.org/download-90.cgi)        |
| MyBatis         | 3.4.1    | [点击访问](https://mybatis.org/mybatis-3/zh/index.html)      |
| JJWT            | 0.11.2   | [点击访问](https://github.com/jwtk/jjwt)                     |
| Knife4j         | 3.0.2    | [点击访问](https://gitee.com/xiaoym/knife4j)                 |

**开发的工具：**

| 工具名称           | 采用版本  | 下载地址                                                     |
| ------------------ | --------- | ------------------------------------------------------------ |
| IntelliJ IDEA      | 2021.1    | [下载地址](https://www.jetbrains.com/idea/download/#section=windows) |
| Maven              | 3.6.3     | [下载地址](https://maven.apache.org/download.cgi)            |
| MySQL              | 5.7.34    | [下载地址](https://dev.mysql.com/downloads/mysql/)           |
| Redis              | 3.2.100   | [下载地址](https://github.com/MicrosoftArchive/redis/tags)   |
| Visual Studio Code | 1.58.2    | [下载地址](https://code.visualstudio.com/)                   |
| Node.js            | 14.17.1   | [下载地址](https://nodejs.org/zh-cn/)                        |
| Java JDK           | 1.8.0_291 | [下载地址](https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html) |

# 目录介绍

```bash
blog-front           网站前台（前端）
blog-manage          网站后台（前端）
blog-server          网站后端（后端）
```

# 项目安装

```bash
# 导入数据库，注意数据库账号和密码
blog.sql
```

```bash
# 克隆项目
git clone https://gitee.com/caochenlei/blog.git
```

blog-server安装及启动：

```bash
# 进入后端
cd blog-server

# 安装依赖
mvn install

# 运行项目
mvn spring-boot:run
```

blog-manage安装及启动：

```bash
# 进入后台
cd blog-manage

# 安装依赖
npm i --legacy-peer-deps

# 运行项目
npm start
```

blog-front安装及启动：

```bash
# 进入前台
cd blog-front

# 安装依赖
npm i --legacy-peer-deps

# 运行项目
npm start
```

```
访问地址：http://localhost:4000/
登录账号：zhangsan
登录密码：123456
```

# 注意事项

**问题一：blog-front如何修改对接后台的地址？**

回答一：找到`blog-front\src\axios\request.ts`，然后进行修改。

**问题二：blog-manage如何修改对接后台的地址？**

回答二：找到`blog-manage\src\axios\request.ts`，然后进行修改。

**回答三：为什么我部署完成以后页面图片缺失？**

回答三：本博客采用腾讯云存储进行图片存取，为了我个人的隐私，我在配置文件中，将密钥设置为XXXX，请设置为您自己的密钥，并将《blog-images》中的存储桶里的图片上传到自己账户的存储桶中，并在配置文件中配置存储信息。如果还是不显示，还需要将数据库中的存储的图片地址的域名更换一下。

如果您有其他的问题，请联系作者或者关注CSDN进行私信。

# 下个版本

- 没有下个版本迭代计划

# 联系作者

Email：[774908833@qq.com](mailto:774908833@qq.com)

CSDN：[https://caochenlei.blog.csdn.net/](https://caochenlei.blog.csdn.net/)